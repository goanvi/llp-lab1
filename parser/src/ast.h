//
// Created by gav66 on 1/4/2024.
//

#ifndef LLP_AST_H
#define LLP_AST_H

#include <stdio.h>
#include <stdbool.h>

#define SINGLE_INDENT 2

enum compare_type {
    CMP_LE = 0,
    CMP_GE,
    CMP_LT,
    CMP_GT,
    CMP_EQ,
    CMP_NE,
};

enum condition_type {
    COND_CMP = 0,
    COND_NOT,
    COND_AND,
    COND_OR,
    COND_CONTAINS
};

enum data_type {
    TYPE_INT = 0,
    TYPE_FLOAT,
    TYPE_TEXT,
    TYPE_BOOLEAN,
};

enum ast_node_type {
    N_INT = 0,
    N_BOOL,
    N_STRING,
    N_FLOAT,
    N_COLUMN_REFERENCE,
    N_COMPARE,
    N_CONDITION,
    N_JOIN,
    N_DELETE_TABLE_QUERY,
    N_COLUMN_DECL,
    N_CREATE_TABLE_QUERY,
    N_SELECT_QUERY,
    N_INSERT_QUERY,
    N_DELETE_QUERY,
    N_LIST,
    N_UPDATE_LIST_ITEM,
    N_UPDATE_QUERY,
};

struct ast_node {
    enum ast_node_type type;
    union {
        struct {
            int value;
        } INT;
        struct {
            bool value;
        } BOOL;
        struct {
            char *value;
        } STRING;
        struct {
            float value;
        } FLOAT;
        struct {
            char *table;
            char *column;
        } COLUMN_REFERENCE;
        struct {
            enum compare_type type;
            struct ast_node *left;
            struct ast_node *right;
        } COMPARE;
        struct {
            enum condition_type type;
            struct ast_node *first;
            struct ast_node *second;
        } CONDITION;
        struct {
            char *variable;
            char *table;
            struct ast_node *left;
            struct ast_node *right;
        } JOIN;
        struct {
            char *table;
        } DELETE_TABLE_QUERY;
        struct {
            enum data_type type;
            char *column;
        } COLUMN_DECL;
        struct {
            char *table;
            struct ast_node *columns;
        } CREATE_TABLE_QUERY;
        struct {
            struct ast_node *selector;
            char *variable;
            char *table;
            struct ast_node *join;
            struct ast_node *where;
        } SELECT_QUERY;
        struct {
            char *table;
            struct ast_node *values;
        } INSERT_QUERY;
        struct {
            char *table;
            struct ast_node *where;
        } DELETE_QUERY;
        struct {
            struct ast_node *value;
            struct ast_node *next;
        } LIST;
        struct {
            char* column;
            struct ast_node *value;
        } UPDATE_LIST_ITEM;
        struct {
            char *table;
            struct ast_node *updateList;
            struct ast_node *where;
        } UPDATE_QUERY;
    } data;
};

void yyerror(struct ast_node **result, char *s, ...);

struct ast_node *parse_string(char *string);

struct ast_node *parse_file(FILE *file);

void free_ast_node(struct ast_node *node);

struct ast_node *new_ast_node(void);

struct ast_node *new_int_ast_node(int value);

struct ast_node *new_float_ast_node(float value);

struct ast_node *new_bool_ast_node(bool value);

struct ast_node *new_string_ast_node(char *value);

struct ast_node *new_column_reference_ast_node(char *tableName, char *columnName);

struct ast_node *new_compare_ast_node(
        enum compare_type type,
        struct ast_node *left,
        struct ast_node *right
);

struct ast_node *new_condition_ast_node(
        enum condition_type type,
        struct ast_node *first,
        struct ast_node *second
);

struct ast_node *new_join_ast_node(char* variable, char *table, struct ast_node *left, struct ast_node *right);

struct ast_node *new_delete_table_query_ast_node(char *table);

struct ast_node *new_column_declaration_ast_node(char *column, enum data_type type);

struct ast_node *new_create_table_query_ast_node(char *table, struct ast_node *columns);

struct ast_node *new_select_query_ast_node(
        char *variable,
        char *table,
        struct ast_node *join,
        struct ast_node *where,
        struct ast_node *selector
);

struct ast_node *new_delete_query_ast_node(char *table, struct ast_node *where);

struct ast_node *new_insert_query_ast_node(
        char *table,
        struct ast_node *values
);

struct ast_node *new_update_list_item_ast_node(char* column, struct ast_node *value);

struct ast_node *new_update_query_ast_node(
        char *table,
        struct ast_node *updateList,
        struct ast_node *where
);

struct ast_node* new_list_ast_node(struct ast_node *item, struct ast_node *next);

void print_indent(int n);

char* compare_type_to_string(enum compare_type type);

char* condition_type_to_string(enum condition_type type);

char* data_type_to_string(enum data_type type);

void print_ast(struct ast_node *tree, int indent);

#endif //LLP_AST_H
