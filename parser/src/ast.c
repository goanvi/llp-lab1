//
// Created by gav66 on 1/4/2024.
//

#include <malloc.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include "ast.h"

struct ast_node *new_ast_node(void) {
    struct ast_node *node = malloc(sizeof(struct ast_node));
    if (node == NULL) {
        return NULL;
    }
    *node = (struct ast_node) {0};
    return node;
}

struct ast_node *new_int_ast_node(int value) {
    struct ast_node *node = new_ast_node();
    node->type = N_INT;
    node->data.INT.value = value;
    return node;
}

struct ast_node *new_float_ast_node(float value) {
    struct ast_node *node = new_ast_node();
    node->type = N_FLOAT;
    node->data.FLOAT.value = value;
    return node;
}

struct ast_node *new_bool_ast_node(bool value) {
    struct ast_node *node = new_ast_node();
    node->type = N_BOOL;
    node->data.BOOL.value = value;
    return node;
}

struct ast_node *new_string_ast_node(char *value) {
    struct ast_node *node = new_ast_node();
    node->type = N_STRING;
    node->data.STRING.value = value;
    return node;
}

struct ast_node *new_column_reference_ast_node(char* tableName, char* columnName) {
    struct ast_node *node = new_ast_node();
    node->type = N_COLUMN_REFERENCE;
    node->data.COLUMN_REFERENCE.table = tableName;
    node->data.COLUMN_REFERENCE.column = columnName;
    return node;
}

struct ast_node *new_compare_ast_node(
        enum compare_type type,
        struct ast_node *left,
        struct ast_node *right
) {
    struct ast_node *node = new_ast_node();
    node->type = N_COMPARE;
    node->data.COMPARE.type = type;
    node->data.COMPARE.left = left;
    node->data.COMPARE.right = right;
    return node;
}

struct ast_node *new_condition_ast_node(
        enum condition_type type,
        struct ast_node *first,
        struct ast_node *second
) {
    struct ast_node *node = new_ast_node();
    node->type = N_CONDITION;
    node->data.CONDITION.type = type;
    node->data.CONDITION.first = first;
    node->data.CONDITION.second = second;
    return node;
}

struct ast_node *new_join_ast_node(char* variable, char* table, struct ast_node* left, struct ast_node* right) {
    struct ast_node *node = new_ast_node();
    node->type = N_JOIN;
    node->data.JOIN.variable = variable;
    node->data.JOIN.table = table;
    node->data.JOIN.left = left;
    node->data.JOIN.right = right;
    return node;
}

struct ast_node *new_delete_table_query_ast_node(char *table) {
    struct ast_node *node = new_ast_node();
    node->type = N_DELETE_TABLE_QUERY;
    node->data.DELETE_TABLE_QUERY.table = table;
    return node;
}

struct ast_node *new_create_table_query_ast_node(char* table, struct ast_node* columns) {
    struct ast_node *node = new_ast_node();
    node->type = N_CREATE_TABLE_QUERY;
    node->data.CREATE_TABLE_QUERY.table = table;
    node->data.CREATE_TABLE_QUERY.columns = columns;
    return node;
}

struct ast_node *new_select_query_ast_node(
        char *variable,
        char *table,
        struct ast_node *join,
        struct ast_node *where,
        struct ast_node *selector
) {
    struct ast_node *node = new_ast_node();
    node->type = N_SELECT_QUERY;
    node->data.SELECT_QUERY.variable = variable;
    node->data.SELECT_QUERY.table = table;
    node->data.SELECT_QUERY.selector = selector;
    node->data.SELECT_QUERY.join = join;
    node->data.SELECT_QUERY.where = where;
    return node;
}

struct ast_node *new_delete_query_ast_node(char *table, struct ast_node *where) {
    struct ast_node *node = new_ast_node();
    node->type = N_DELETE_QUERY;
    node->data.DELETE_QUERY.table = table;
    node->data.DELETE_QUERY.where = where;
    return node;
}

struct ast_node *new_insert_query_ast_node(
        char *table,
        struct ast_node *values
) {
    struct ast_node *node = new_ast_node();
    node->type = N_INSERT_QUERY;
    node->data.INSERT_QUERY.table = table;
    node->data.INSERT_QUERY.values = values;
    return node;
}

struct ast_node *new_update_list_item_ast_node(char* column, struct ast_node *value) {
    struct ast_node *node = new_ast_node();
    node->type = N_UPDATE_LIST_ITEM;
    node->data.UPDATE_LIST_ITEM.column = column;
    node->data.UPDATE_LIST_ITEM.value = value;
    return node;
}

struct ast_node *new_update_query_ast_node(
        char *table,
        struct ast_node *updateList,
        struct ast_node *where
) {
    struct ast_node *node = new_ast_node();
    node->type = N_UPDATE_QUERY;
    node->data.UPDATE_QUERY.table = table;
    node->data.UPDATE_QUERY.updateList = updateList;
    node->data.UPDATE_QUERY.where = where;
    return node;
}

struct ast_node *new_column_declaration_ast_node(char *column, enum data_type type) {
    struct ast_node *node = new_ast_node();
    node->type = N_COLUMN_DECL;
    node->data.COLUMN_DECL.column = column;
    node->data.COLUMN_DECL.type = type;
    return node;
}

struct ast_node* new_list_ast_node(struct ast_node *item, struct ast_node *next) {
    struct ast_node *node = new_ast_node();
    node->type = N_LIST;
    node->data.LIST.value = item;
    node->data.LIST.next = next;
    return node;
}

void free_ast_node(struct ast_node *node) {
    if (node == NULL) {
        return;
    }
    switch (node->type) {
        case N_INT:
            break;
        case N_BOOL:
            break;
        case N_FLOAT:
            break;
        case N_STRING:
            free(node->data.STRING.value);
            break;
        case N_COMPARE:
            free_ast_node(node->data.COMPARE.right);
            free_ast_node(node->data.COMPARE.left);
            break;
        case N_CONDITION:
            free_ast_node(node->data.CONDITION.first);
            free_ast_node(node->data.CONDITION.second);
            break;
        case N_COLUMN_REFERENCE:
            free(node->data.COLUMN_REFERENCE.table);
            free(node->data.COLUMN_REFERENCE.column);
            break;
        case N_DELETE_TABLE_QUERY:
            free(node->data.DELETE_TABLE_QUERY.table);
            break;
        case N_CREATE_TABLE_QUERY:
            free(node->data.CREATE_TABLE_QUERY.table);
            free_ast_node(node->data.CREATE_TABLE_QUERY.columns);
            break;
        case N_SELECT_QUERY:
            free(node->data.SELECT_QUERY.table);
            free_ast_node(node->data.SELECT_QUERY.join);
            free_ast_node(node->data.SELECT_QUERY.where);
            free_ast_node(node->data.SELECT_QUERY.selector);
            break;
        case N_INSERT_QUERY:
            free_ast_node(node->data.INSERT_QUERY.values);
            free(node->data.INSERT_QUERY.table);
            break;
        case N_DELETE_QUERY:
            free_ast_node(node->data.DELETE_QUERY.where);
            free(node->data.DELETE_QUERY.table);
            break;
        case N_UPDATE_QUERY:
            free(node->data.UPDATE_QUERY.table);
            free_ast_node(node->data.UPDATE_QUERY.where);
            free_ast_node(node->data.UPDATE_QUERY.updateList);
            break;
        case N_JOIN:
            free_ast_node(node->data.JOIN.right);
            free_ast_node(node->data.JOIN.left);
            free(node->data.JOIN.table);
            break;
        case N_LIST:
            free_ast_node(node->data.LIST.value);
            free_ast_node(node->data.LIST.next);
            break;
        case N_UPDATE_LIST_ITEM:
            free_ast_node(node->data.UPDATE_LIST_ITEM.value);
            free(node->data.UPDATE_LIST_ITEM.column);
            break;
        case N_COLUMN_DECL:
            free(node->data.COLUMN_DECL.column);
            break;
    }
    free(node);
}

extern int yylineno;

typedef struct yy_buffer_state *YY_BUFFER_STATE;

void yy_switch_to_buffer(YY_BUFFER_STATE new_buffer);

extern int yyparse(struct ast_node **result);

extern FILE *yyin;

extern void yy_delete_buffer(YY_BUFFER_STATE buffer);

extern YY_BUFFER_STATE yy_scan_string(char *str);

void yyerror(struct ast_node **result, char *s, ...) {
    va_list ap;
    va_start(ap, s);
    fprintf(stdout, "ERROR: syntax error\n");
    free_ast_node(*result);
    *result = NULL;
}

struct ast_node *parse_string(char *string) {
    YY_BUFFER_STATE buffer = yy_scan_string(string);
    yy_switch_to_buffer(buffer);
    struct ast_node *nodeRef = NULL;
    yyparse(&nodeRef);
    yy_delete_buffer(buffer);
    return nodeRef;
}

struct ast_node *parse_file(FILE *file) {
    yyin = file;
    struct ast_node *nodeRef = NULL;
    yyparse(&nodeRef);
    return nodeRef;
}

void print_indent(int n) {
    for (int i = 0; i < n; i++) {
        if (i%2==0){
            printf("|");
        } else {
            printf(" ");
        }
    }
}

char* compare_type_to_string(enum compare_type type) {
    switch (type) {
        case CMP_LE:
            return "LESS EQUALS";
        case CMP_GE:
            return "GREATER EQUALS";
        case CMP_LT:
            return "LESS";
        case CMP_GT:
            return "GREATER";
        case CMP_EQ:
            return "EQUALS";
        case CMP_NE:
            return "NOT EQUALS";
        default:
            assert(0);
    }
}

char* condition_type_to_string(enum condition_type type) {
    switch (type) {
        case COND_CMP:
            return "COMPARE";
        case COND_NOT:
            return "NOT";
        case COND_AND:
            return "AND";
        case COND_OR:
            return "OR";
        case COND_CONTAINS:
            return "CONTAINS";
        default:
            assert(0);
    }
}

char* data_type_to_string(enum data_type type) {
    switch (type) {
        case TYPE_INT:
            return "INT";
        case TYPE_FLOAT:
            return "FLOAT";
        case TYPE_TEXT:
            return "TEXT";
        case TYPE_BOOLEAN:
            return "BOOLEAN";
        default:
            assert(0);
    }
}

void print_ast(struct ast_node *tree, int indent) {
    if (tree == NULL) {
        return;
    }
    print_indent(indent);
    switch (tree->type) {
        case N_INT:
            printf("INT: %d \n", tree->data.INT.value);
            break;
        case N_FLOAT:
            printf("FLOAT: %f \n", tree->data.FLOAT.value);
            break;
        case N_BOOL:
            printf("BOOLEAN: %s \n", tree->data.BOOL.value ? "TRUE" : "FALSE");
            break;
        case N_STRING:
            printf("STRING: %s \n", tree->data.STRING.value);
            break;
        case N_COMPARE:
            printf("COMPARE TYPE: %s \n", compare_type_to_string(tree->data.COMPARE.type));
            print_ast(tree->data.COMPARE.left, indent + SINGLE_INDENT);
            print_ast(tree->data.COMPARE.right, indent + SINGLE_INDENT);
            break;
        case N_CONDITION:
            printf("CONDITION TYPE: %s \n", condition_type_to_string(tree->data.CONDITION.type));
            print_ast(tree->data.CONDITION.first, indent + SINGLE_INDENT);
            print_ast(tree->data.CONDITION.second, indent + SINGLE_INDENT);
            break;
        case N_COLUMN_REFERENCE:
            printf("COLUMN REFERENCE\n");
            print_indent(indent + SINGLE_INDENT);
            printf("VARIABLE: %s, COLUMN: %s\n", tree->data.COLUMN_REFERENCE.table,
                   tree->data.COLUMN_REFERENCE.column);
            break;
        case N_JOIN:
            printf("JOIN (VARIABLE: %s) IN (TABLE: %s) ON \n", tree->data.JOIN.variable, tree->data.JOIN.table);
            print_ast(tree->data.JOIN.left, indent + SINGLE_INDENT);
            print_ast(tree->data.JOIN.right, indent + SINGLE_INDENT);
            break;
        case N_DELETE_TABLE_QUERY:
            printf("DROP TABLE: %s\n", tree->data.DELETE_TABLE_QUERY.table);
            break;
        case N_COLUMN_DECL:
            printf("COLUMN DECLARATION\n");
            print_indent(indent + SINGLE_INDENT);
            printf("NAME: %s, TYPE: %s\n", tree->data.COLUMN_DECL.column,
                   data_type_to_string(tree->data.COLUMN_DECL.type));
            break;
        case N_CREATE_TABLE_QUERY:
            printf("CREATE TABLE: %s\n", tree->data.CREATE_TABLE_QUERY.table);
            print_ast(tree->data.CREATE_TABLE_QUERY.columns, indent + SINGLE_INDENT);
            break;
        case N_SELECT_QUERY:
            printf("SELECT FROM (VARIABLE: %s) IN (TABLE: %s) \n",tree->data.SELECT_QUERY.variable ,tree->data.SELECT_QUERY.table);
            indent += SINGLE_INDENT;
            if (tree->data.SELECT_QUERY.join != NULL){
                print_indent(indent);
                printf("JOIN LIST \n");
                print_ast(tree->data.SELECT_QUERY.join, indent + SINGLE_INDENT);
            }
            if (tree->data.SELECT_QUERY.where != NULL){
                print_indent(indent);
                printf("WHERE \n");
                print_ast(tree->data.SELECT_QUERY.where, indent + SINGLE_INDENT);
            }
            print_indent(indent);
            printf("SELECTOR LIST \n");
            print_ast(tree->data.SELECT_QUERY.selector, indent + SINGLE_INDENT);
            break;
        case N_INSERT_QUERY:
            printf("INSERT INTO TABLE: %s\n", tree->data.INSERT_QUERY.table);
            indent += SINGLE_INDENT;
            print_indent(indent);
            printf("VALUES LIST \n");
            print_ast(tree->data.INSERT_QUERY.values, indent + SINGLE_INDENT);
            break;
        case N_DELETE_QUERY:
            printf("DELETE FROM TABLE: %s\n", tree->data.DELETE_QUERY.table);
            print_ast(tree->data.DELETE_QUERY.where, indent + SINGLE_INDENT);
            break;
        case N_LIST:
            printf("LIST ITEM \n");
            print_ast(tree->data.LIST.value, indent + SINGLE_INDENT);
            print_ast(tree->data.LIST.next, indent);
            break;
        case N_UPDATE_LIST_ITEM:
            printf("SET COLUMN: %s \n", tree->data.UPDATE_LIST_ITEM.column);
            print_ast(tree->data.UPDATE_LIST_ITEM.value, indent + SINGLE_INDENT);
            break;
        case N_UPDATE_QUERY:
            printf("UPDATE TABLE: %s \n", tree->data.UPDATE_QUERY.table);
            indent += SINGLE_INDENT;
            print_indent(indent);
            printf("UPDATE LIST \n");
            print_ast(tree->data.UPDATE_QUERY.updateList, indent + SINGLE_INDENT);
            print_indent(indent);
            printf("WHERE \n");
            print_ast(tree->data.UPDATE_QUERY.where, indent + SINGLE_INDENT);
            break;
    }
}