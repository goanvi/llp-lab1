%{
#include <stdio.h>
#include <stdlib.h>
#include "ast.h"
extern int yylex(void);
%}

%parse-param { struct ast_node **tree }

%union {
    char* sval;
	float fval;
	int ival;
	int bval;
	struct ast_node *ast_node;
}

%token<sval> TOKEN_STRING TOKEN_NAME
%token<fval> TOKEN_FLOAT
%token<ival> TOKEN_INT TOKEN_TYPE_BOOLEAN TOKEN_TYPE_TEXT TOKEN_TYPE_FLOAT TOKEN_TYPE_INT
%token<ival> TOKEN_LE TOKEN_GE TOKEN_LT TOKEN_GT TOKEN_EQ TOKEN_NE
%token<bval> TOKEN_BOOL

%token TOKEN_PAR_OPEN TOKEN_PAR_CLOSE
%token TOKEN_COMMA TOKEN_DOT END_OF_STATEMENT

%token TOKEN_NOT TOKEN_OR TOKEN_AND TOKEN_LIMIT TOKEN_OFFSET TOKEN_VALUES TOKEN_CREATE TOKEN_IN
%token TOKEN_TABLE TOKEN_ON TOKEN_JOIN TOKEN_WHERE TOKEN_DELETE TOKEN_INTO TOKEN_SET TOKEN_DROP
%token TOKEN_INSERT TOKEN_UPDATE TOKEN_SELECT TOKEN_FROM TOKEN_EQUALS TOKEN_CONTAINS

%left TOKEN_LE TOKEN_GE TOKEN_LT TOKEN_GT TOKEN_EQ TOKEN_NE

%type<ival> COMP_OPERATOR DATA_TYPE
%type<ast_node> VALUE_INT VALUE_BOOL VALUE_FLOAT VALUE_STRING VALUE
%type<ast_node> COMP_OPERAND COMP_CONDITION CONDITION JOIN JOINS WHERE_SECTION SELECTOR
%type<ast_node> COL_DECLARATION COL_DECLARATION_LIST
%type<ast_node> COL_LIST VALUES_LIST UPDATE_LIST TABLE_COLUMN
%type<ast_node> QUERIES QUERY SELECT_QUERY INSERT_QUERY DELETE_QUERY UPDATE_QUERY CREATE_TABLE_QUERY DELETE_TABLE_QUERY

%start QUERIES

%%

VALUE_INT: TOKEN_INT { $$ = new_int_ast_node($1); };

VALUE_BOOL: TOKEN_BOOL { $$ = new_bool_ast_node($1); };

VALUE_FLOAT: TOKEN_FLOAT { $$ = new_float_ast_node($1); };

VALUE_STRING: TOKEN_STRING { $$ = new_string_ast_node($1); };

VALUE:
    VALUE_INT |
    VALUE_BOOL |
    VALUE_FLOAT |
    VALUE_STRING;

COMP_CONDITION:
    COMP_OPERAND COMP_OPERATOR COMP_OPERAND {
        $$ = new_compare_ast_node($2, $1, $3);
    };

COMP_OPERAND:
    TABLE_COLUMN |
    VALUE;

CONDITION:
    TOKEN_PAR_OPEN CONDITION TOKEN_PAR_CLOSE { $$ = $2; } |
    COMP_CONDITION { $$ = new_condition_ast_node(COND_CMP, $1, NULL); } |
    CONDITION TOKEN_OR CONDITION { $$ = new_condition_ast_node(COND_OR, $1, $3); } |
    CONDITION TOKEN_AND CONDITION { $$ = new_condition_ast_node(COND_AND, $1, $3); } |
    TABLE_COLUMN TOKEN_DOT TOKEN_CONTAINS TOKEN_PAR_OPEN VALUE_STRING TOKEN_PAR_CLOSE {
        $$ = new_condition_ast_node(COND_CONTAINS, $1, $5);
    } |
    TOKEN_NOT CONDITION { $$ = new_condition_ast_node(COND_NOT, $2, NULL); };

COMP_OPERATOR:
    TOKEN_LE |
    TOKEN_GE |
    TOKEN_LT |
    TOKEN_GT |
    TOKEN_EQ |
    TOKEN_NE;

JOIN:
    TOKEN_JOIN TOKEN_NAME TOKEN_IN TOKEN_NAME TOKEN_ON TABLE_COLUMN TOKEN_EQUALS TABLE_COLUMN {
        $$ = new_join_ast_node($2, $4, $6, $8);
    };

JOINS:
    { $$ = NULL; } |
    JOIN JOINS {
        $$ = new_list_ast_node($1, $2);
    };

WHERE_SECTION:
    { $$ = NULL; } |
    TOKEN_WHERE CONDITION { $$ = $2; };

DELETE_TABLE_QUERY:
    TOKEN_DROP TOKEN_TABLE TOKEN_NAME { $$ = new_delete_table_query_ast_node($3); };

TABLE_COLUMN:
    TOKEN_NAME TOKEN_DOT TOKEN_NAME { $$ = new_column_reference_ast_node($1, $3); };

DATA_TYPE:
    TOKEN_TYPE_BOOLEAN |
    TOKEN_TYPE_TEXT |
    TOKEN_TYPE_FLOAT |
    TOKEN_TYPE_INT;

COL_DECLARATION:
    TOKEN_NAME DATA_TYPE { $$ = new_column_declaration_ast_node($1, $2); };

COL_DECLARATION_LIST:
    COL_DECLARATION {
        $$ = new_list_ast_node($1, NULL);
    } |
    COL_DECLARATION TOKEN_COMMA COL_DECLARATION_LIST {
        $$ = new_list_ast_node($1, $3);
    };

CREATE_TABLE_QUERY:
    TOKEN_CREATE TOKEN_TABLE TOKEN_NAME TOKEN_PAR_OPEN COL_DECLARATION_LIST TOKEN_PAR_CLOSE {
        $$ = new_create_table_query_ast_node($3, $5);
    };

DELETE_QUERY:
    TOKEN_DELETE TOKEN_FROM TOKEN_NAME WHERE_SECTION {
        $$ = new_delete_query_ast_node($3, $4);
    };

COL_LIST:
    TOKEN_NAME {
        $$ = new_list_ast_node(new_string_ast_node($1), NULL);
    } | TOKEN_NAME TOKEN_COMMA COL_LIST {
        $$ = new_list_ast_node(new_string_ast_node($1), $3);
    };

VALUES_LIST:
    VALUE {
        $$ = new_list_ast_node($1, NULL);
    } | VALUE TOKEN_COMMA VALUES_LIST {
        $$ = new_list_ast_node($1, $3);
    };

INSERT_QUERY:
    TOKEN_INSERT TOKEN_INTO TOKEN_NAME TOKEN_VALUES TOKEN_PAR_OPEN VALUES_LIST TOKEN_PAR_CLOSE {
        $$ = new_insert_query_ast_node($3, $6);
    };

UPDATE_LIST:
    TOKEN_NAME TOKEN_EQ VALUE {
        $$ = new_list_ast_node(new_update_list_item_ast_node($1, $3), NULL);
    } | TOKEN_NAME TOKEN_EQ VALUE TOKEN_COMMA UPDATE_LIST {
        $$ = new_list_ast_node(new_update_list_item_ast_node($1, $3), $5);
    };

UPDATE_QUERY:
    TOKEN_UPDATE TOKEN_NAME TOKEN_SET UPDATE_LIST WHERE_SECTION {
        $$ = new_update_query_ast_node($2, $4, $5);
    };

SELECTOR:
    TABLE_COLUMN {
        $$ = new_list_ast_node($1, NULL);
    } | TABLE_COLUMN TOKEN_COMMA SELECTOR {
        $$ = new_list_ast_node($1, $3);
    };


SELECT_QUERY:
     TOKEN_FROM TOKEN_NAME TOKEN_IN TOKEN_NAME JOINS WHERE_SECTION TOKEN_SELECT SELECTOR{
        $$ = new_select_query_ast_node($2, $4, $5, $6, $8);
    };

QUERIES:
    { $$ = NULL; } |
    QUERY END_OF_STATEMENT QUERIES {
        $$ = new_list_ast_node($1, $3);
        *tree = $$;
    };

QUERY:
    CREATE_TABLE_QUERY |
    DELETE_TABLE_QUERY |
    INSERT_QUERY |
    DELETE_QUERY |
    SELECT_QUERY |
    UPDATE_QUERY ;
%%
