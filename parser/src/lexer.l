%option noyywrap

%{
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "parser.h"
#include "ast.h"
%}

%%
    /* literal */
(?i:select) { return TOKEN_SELECT; }
(?i:not) { return TOKEN_NOT; }
(?i:float) { yylval.ival = TYPE_FLOAT; return TOKEN_TYPE_FLOAT; }
(?i:offset) { return TOKEN_OFFSET; }
(?i:update) { return TOKEN_UPDATE; }
(?i:join) { return TOKEN_JOIN; }
(?i:boolean) { yylval.ival = TYPE_BOOLEAN; return TOKEN_TYPE_BOOLEAN; }
(?i:delete) { return TOKEN_DELETE; }
(?i:drop) { return TOKEN_DROP; }
(?i:insert) { return TOKEN_INSERT; }
(?i:values) { return TOKEN_VALUES; }
(?i:on) { return TOKEN_ON; }
(?i:create) { return TOKEN_CREATE; }
(?i:table) { return TOKEN_TABLE; }
(?i:limit) { return TOKEN_LIMIT; }
(?i:int) { yylval.ival = TYPE_INT; return TOKEN_TYPE_INT; }
(?i:set) { return TOKEN_SET; }
(?i:into) { return TOKEN_INTO; }
(?i:from) { return TOKEN_FROM; }
(?i:where) { return TOKEN_WHERE; }
(?i:contains) { return TOKEN_CONTAINS; }
(?i:or) { return TOKEN_OR; }
(?i:text) { yylval.ival = TYPE_TEXT; return TOKEN_TYPE_TEXT; }
(?i:and) { return TOKEN_AND; }
(?i:equals) { return TOKEN_EQUALS; }
(?i:in) { return TOKEN_IN; }
(?i:true) { yylval.bval = 1; return TOKEN_BOOL; }
(?i:false) { yylval.bval = 0; return TOKEN_BOOL; }

    /* regex */
[A-Za-z][A-Za-z0-9_]* { yylval.sval = strdup(yytext); return TOKEN_NAME; }
[0-9]+ { yylval.ival = atoi(yytext); return TOKEN_INT; }
[0-9]+\.[0-9]+ { yylval.fval = atof(yytext); return TOKEN_FLOAT; }
\'(\\.|[^'\\])*\' { yytext[strlen(yytext) - 1] = '\0'; yylval.sval = strdup(yytext + 1); return TOKEN_STRING; }

    /* characters */
"<" { yylval.ival = CMP_LT; return TOKEN_LT; }
">" { yylval.ival = CMP_GT; return TOKEN_GT; }
"<=" { yylval.ival = CMP_LE; return TOKEN_LE; }
">=" { yylval.ival = CMP_GE; return TOKEN_GE; }
"!=" { yylval.ival = CMP_NE; return TOKEN_NE; }
"=" { yylval.ival = CMP_EQ; return TOKEN_EQ; }
";" { return END_OF_STATEMENT; }
"," { return TOKEN_COMMA; }
"." { return TOKEN_DOT; }
")" { return TOKEN_PAR_CLOSE; }
"(" { return TOKEN_PAR_OPEN; }
"||" { return TOKEN_OR; }
"&&" { return TOKEN_AND; }

    /* ignore */
[ \n\t\r];
. {};
%%
