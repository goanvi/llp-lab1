//
// Created by gav66 on 1/4/2024.
//

#include <string.h>
#include "ast.h"

int main(int argc, char * argv[]) {
    if (argc != 2) {
        return -1;
    }
    char* filename = argv[1];
    FILE *f = NULL;
    if (strcmp(filename, "stdin") == 0) {
        f = stdin;
    } else {
        f = fopen(filename, "r");
    }
    if (f == NULL) {
        printf("Can't read file: %s \n", filename);
        return -1;
    }
    struct ast_node *tree = parse_file(f);
    if (tree == NULL) {
        printf("Invalid input");
    } else {
        print_ast(tree, 0);
    }
    fclose(f);
    free_ast_node(tree);
    return 0;
}