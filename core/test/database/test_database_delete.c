//
// Created by gav66 on 12/01/2023.
//

#include <stdio.h>
#include <time.h>
#include "database/database.h"
#include "table/condition/condition.h"
#include "table/table_client.h"

void delete(struct database *db) {
    struct where_condition *condition = where_condition_compare(
            COMPARE_EQ,
            operand_column("test", "bool"),
            operand_literal_bool(true)
    );
    struct query query = {
            .table = "test",
            .where = condition,
            .joins = NULL
    };
    clock_t begin = clock();
    enum table_status status = table_delete(db, query);
    clock_t end = clock();
    double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
    where_condition_free(condition);
    printf("%f", time_spent);
}

int main() {
    struct database *db = open_db("test.txt");
    if (db != NULL) {
        delete(db);
        save_db(&db);
        return 0;
    }
    return -1;
}
