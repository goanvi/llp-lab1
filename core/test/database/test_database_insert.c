//
// Created by gav66 on 12/01/2023.
//

#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <stdlib.h>
#include "database/database.h"
#include "table/row_builder/row_builder.h"
#include "table/table_builder/table_builder.h"
#include "table/table_client.h"

void insert(struct database *db, int n, char *table_name) {
    struct batch_builder batch = batch_builder_init(n);
    for (int i = 0; i < n; i++) {
        int integer;
        float floating;
        int boolean;
        char string[64];
        scanf("%d", &integer);
        scanf("%s", string);
        scanf("%d", &boolean);
        scanf("%f", &floating);
        struct row_builder row = row_builder_init(4);
        row_builder_add(&row, column_int(integer));
        row_builder_add(&row, column_string(string));
        row_builder_add(&row, column_bool(!!boolean));
        row_builder_add(&row, column_float(floating));
        batch_builder_add(&batch, row);
    }

    clock_t begin = clock();
    table_insert(db, table_name, batch);
    clock_t end = clock();
    double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
    printf("%f", time_spent);

    batch_builder_free(&batch);
}

void table_create(struct database *db, char *table_name) {
    struct table_builder *scheme_builder = table_builder_init(table_name);
    table_builder_add_column(scheme_builder, "int", DATA_TYPE_INT);
    table_builder_add_column(scheme_builder, "string", DATA_TYPE_STRING);
    table_builder_add_column(scheme_builder, "bool", DATA_TYPE_BOOL);
    table_builder_add_column(scheme_builder, "float", DATA_TYPE_FLOAT);
    create_table(db, scheme_builder);
    table_builder_free(&scheme_builder);
}

int main(int argc, char *argv[]) {
    assert(argc == 4);
    int n = atoi(argv[1]);
    enum database_init mode = atoi(argv[2]);
    char *table_name = argv[3];
    struct database *db = mode == DATABASE_CREATE ? create_db("test.txt") : open_db("test.txt");
    if (db != NULL) {
        //????
        if (mode == DATABASE_CREATE) {
            table_create(db, table_name);
        }
        insert(db,n,table_name);
        save_db(&db);
        return 0;
    }
    return -1;
}
