//
// Created by gav66 on 12/01/2023.
//

#include <stdio.h>
#include <time.h>
#include "database/database.h"
#include "table/table_client.h"
#include "table/condition/condition.h"

void print_row(struct row row) {
    for (size_t i = 0; i < row.size; i++) {
        struct row_column column = row.columns[i];
        switch (column.type) {
            case DATA_TYPE_INT:
                printf("%d ", column.value.val_int);
                break;
            case DATA_TYPE_FLOAT:
                printf("%f ", column.value.val_float);
                break;
            case DATA_TYPE_STRING:
                printf("%s ", column.value.val_string);
                break;
            case DATA_TYPE_BOOL:
                printf("%d ", column.value.val_bool);
                break;
        }
    }
    printf(" \n");
}

void selecting(struct database *db) {
    struct query query = {
            .table = "test",
            .where = where_condition_and(
                    where_condition_compare(
                            COMPARE_EQ,
                            operand_column("test", "bool"),
                            operand_literal_bool(true)
                    ),
                    where_condition_compare(
                            COMPARE_GE,
                            operand_column("test", "float"),
                            operand_literal_float(0.5f)
                    )
            ),
            .joins = NULL
    };
    struct selector_builder *selector = selector_builder_init();
    selector_builder_add(selector, "test", "int");
    selector_builder_add(selector, "test", "string");
    selector_builder_add(selector, "test", "bool");
    selector_builder_add(selector, "test", "float");

    clock_t begin = clock();

    struct result_view *view = table_select(db, query, selector);
    if (view == NULL) {
        printf("Can't create select \n");
        return;
    }
    int count = 0;
    while (!result_view_is_empty(view)) {
        struct row row = result_view_get(view);
//        print_row(row);
        row_free(row);
        result_view_next(view);
        count++;
    }
//    printf("count: %d \n", count);
    result_view_free(&view);

    clock_t end = clock();
    double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
    printf("%f", time_spent);

    selector_builder_free(&selector);
}

int main(void) {
    struct database *db = open_db("test.txt");
    if (db != NULL) {
        selecting(db);
        save_db(&db);
        return 0;
    }
    return -1;
}
