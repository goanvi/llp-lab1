//
// Created by gav66 on 12/01/2023.
//

#include <stdio.h>
#include <time.h>
#include "database/database.h"
#include "table/table.h"
#include "table/join/join.h"
#include "table/table_client.h"

void print_row(struct row row) {
    for (size_t i = 0; i < row.size; i++) {
        struct row_column column = row.columns[i];
        switch (column.type) {
            case DATA_TYPE_INT:
                printf("%d ", column.value.val_int);
                break;
            case DATA_TYPE_FLOAT:
                printf("%f ", column.value.val_float);
                break;
            case DATA_TYPE_STRING:
                printf("%s ", column.value.val_string);
                break;
            case DATA_TYPE_BOOL:
                printf("%d ", column.value.val_bool);
                break;
        }
    }
    printf(" \n");
}

void selecting(struct database *db) {
    struct join_condition_list *join_builder = join_builder_init();
    join_builder_add(&join_builder,
                     table_column_of(
                             "test",
                             "int"
                     ),
                     table_column_of(
                             "test2",
                             "int"
                     )
    );
    struct query query = {
            .table = "test",
            .where = NULL,
            .joins = join_builder
    };
    struct selector_builder *selector = selector_builder_init();
    selector_builder_add(selector, "test", "int");
    selector_builder_add(selector, "test", "string");
    selector_builder_add(selector, "test", "bool");
    selector_builder_add(selector, "test", "float");
    selector_builder_add(selector, "test2", "int");
    selector_builder_add(selector, "test2", "string");
    selector_builder_add(selector, "test2", "bool");
    selector_builder_add(selector, "test2", "float");

    clock_t begin = clock();

    struct result_view *view = table_select(db, query, selector);
    if (NULL == view) {
        printf("Can't create select \n");
        return;
    }
    while (!result_view_is_empty(view)) {
        struct row row = result_view_get(view);
        print_row(row);
        row_free(row);
        result_view_next(view);
    }
    result_view_free(&view);
    clock_t end = clock();
    double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
    printf("%f", time_spent);
    selector_builder_free(&selector);
    join_builder_free(&join_builder);
}

int main(void) {
    struct database *db = open_db("test.txt");
    if (db != NULL){
        selecting(db);
        save_db(&db);
        return 0;
    }
    return -1;
}
