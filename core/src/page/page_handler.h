//
// Created by gav66 on 10/4/2023.
//

#ifndef LLP_PAGE_HANDLER_H
#define LLP_PAGE_HANDLER_H
#define PAGE_SIZE 4096

#include <stdint.h>
#include "../database/database.h"

//const int64_t PAGE_SIZE = 4096;

enum page_status {
    PAGE_SUCCESS = 0,
    PAGE_WRITE_ERROR,
    PAGE_READ_ERROR,
    PAGE_CREATE_ERROR,
    PAGE_ERROR
};

struct file_page_header { // заголовок страницы
    int64_t next_page;
    int64_t prev_page;
    int64_t end_page;
};

struct page {
    struct file_page_header header;
    int64_t addr;
    int8_t *data;
};

struct page_list {
    struct page *page;
    unsigned short in_use;
    struct page_list *next_page;
    struct page_list *prev_page;
};


struct page *get_page(struct database *db, int64_t page_addr);
void stop_using_page(struct database *db, struct page **page);
//void add_to_page_list(struct database *db, struct page *page);
//enum page_status create_page(FILE *file, struct page *page);
enum page_status link_pages(struct page *cur_page, struct page *next_page);
//struct page *alloc_page();
//void free_page(struct page **page);
enum page_status read_page(FILE *file, int64_t addr, struct page *page);
enum page_status write_page(FILE *file, struct page *page);
struct page *get_empty_page(struct database *db);
void set_empty_page(struct database *db, struct page *empty_page);

#endif //LLP_PAGE_HANDLER_H
