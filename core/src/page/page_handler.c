//
// Created by gav66 on 10/4/2023.
//

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <assert.h>
#include "page_handler.h"
#include "../file/file_handler.h"


enum page_status link_pages(struct page *cur_page, struct page *next_page) {
    // TODO убрать assert, только для дебага
    assert(cur_page != NULL && next_page != NULL);
    if (cur_page != NULL && next_page != NULL) {
        cur_page->header.next_page = next_page->addr;
        next_page->header.prev_page = cur_page->addr;
        return PAGE_SUCCESS;
    } else {
        return PAGE_ERROR;
    }
}

struct page *alloc_page() {
    int8_t *page_data = malloc(PAGE_SIZE);
    if (page_data != NULL) {
        struct page *page = malloc(sizeof(struct page));
        if (page != NULL) {
            page->data = page_data;
            return page;
        } else {
            free(page_data);
            return NULL;
        }
    } else {
        return NULL;
    }
}

void free_page(struct page **page) {
    assert(page != NULL && *page != NULL && (*page)->data != NULL);
    free((*page)->data);
    free(*page);
    *page = NULL;
}


enum page_status write_page(FILE *file, struct page *page) {
    assert(file != NULL && page != NULL && page->data != NULL);
    memcpy(page->data, &page->header, sizeof(struct file_page_header));
    if (file_write(file, page->addr, page->data, PAGE_SIZE) == FILE_SUCCESS) {
        return PAGE_SUCCESS;
    } else {
        return PAGE_WRITE_ERROR;
    }
}


enum page_status read_page(FILE *file, int64_t addr, struct page *page) {
    assert(file != NULL && page != NULL && page->data != NULL);
    if (file_read(file, addr, page->data, PAGE_SIZE) == FILE_SUCCESS) {
        page->addr = addr;
        struct file_page_header header;
        memcpy(&header, page->data, sizeof(struct file_page_header));
        page->header = header;
        return PAGE_SUCCESS;
    } else {
        return PAGE_READ_ERROR;
    }
}


enum page_status create_page(FILE *file, struct page *page) {
    assert(file != NULL && page != NULL && page->data != NULL);
    if (fseek(file, 0, SEEK_END) == 0) {
        int64_t addr = ftell(file);
        page->addr = PAGE_SIZE * (addr / PAGE_SIZE);
        page->header.end_page = sizeof(struct file_page_header);
        page->header.next_page = 0;
        page->header.prev_page = 0;
        memcpy(page->data, &page->header, sizeof(struct file_page_header));
        if (file_write(file, page->addr, page->data, PAGE_SIZE) == FILE_SUCCESS) {
            return PAGE_SUCCESS;
        } else {
            return PAGE_CREATE_ERROR;
        }
    } else {
        return PAGE_CREATE_ERROR;
    }
}

void add_to_page_list(struct database *db, struct page *page) {
    assert(db != NULL);
    if (page != NULL) {
        struct page_list *node = malloc(sizeof(struct page_list));
        if (node != NULL) {
            node->next_page = NULL;
            node->in_use = 1;
            node->page = page;
            if (db->allocated_pages == NULL) {
                node->prev_page = NULL;
                db->allocated_pages = node;
            } else {
                struct page_list *list = db->allocated_pages;
                while (list->next_page != NULL) {
                    list = list->next_page;
                }
                node->prev_page = list;
                list->next_page = node;
            }
        }
    }
}

struct page *get_page(struct database *db, int64_t page_addr) {
    assert(db != NULL);
    struct page_list *list = db->allocated_pages;
    page_addr = (page_addr / PAGE_SIZE) * PAGE_SIZE;
    if (page_addr != 0) {
        while (list != NULL) {
            if (list->page->addr == page_addr) {
                list->in_use++;
                return list->page;
            } else {
                list = list->next_page;
            }
        }
        struct page *page = alloc_page();
        if (page != NULL) {
            if (read_page(db->file, page_addr, page) == PAGE_SUCCESS) {
                add_to_page_list(db, page);
                return page;
            }
        }
    }
    return NULL;
}

void stop_using_page(struct database *db, struct page **page) {
    assert(db != NULL);
    if (page != NULL && *page != NULL && db->allocated_pages != NULL) {
        struct page_list *list = db->allocated_pages;
        while (list != NULL) {
            if (list->page->addr == (*page)->addr) {
                list->in_use--;
                if (list->in_use == 0) {
                    struct page_list *prev_node = list->prev_page;
                    struct page_list *next_node = list->next_page;
                    if (list == db->allocated_pages) {
                        db->allocated_pages = next_node;
                    }
                    if (next_node != NULL) {
                        next_node->prev_page = prev_node;
                    }
                    if (prev_node != NULL) {
                        prev_node->next_page = next_node;
                    }
                    //TODO: как обрабатывать случай если не смог сохранить данные в файл
                    if (write_page(db->file, list->page) == PAGE_SUCCESS) {
                        free_page(&list->page);
                        free(list);
                        *page = NULL;
                    }
                }
                return;
            } else {
                list = list->next_page;
            }
        }
    }
}

struct page *get_empty_page(struct database *db) {
    assert(db != NULL);
    if (db->header->empty_pages > 0) {
        struct page *page = get_page(db, db->header->empty_pages);
        if (page != NULL) {
            db->header->empty_pages = page->header.next_page;
        }
        page->header.next_page = 0;
        page->header.prev_page = 0;
        page->header.end_page = sizeof(struct file_page_header);
        return page;
    } else {
        struct page *new_page = alloc_page();
        if (new_page != NULL) {
            if (create_page(db->file, new_page) == PAGE_SUCCESS) {
                add_to_page_list(db, new_page);
                return new_page;
            } else {
                free_page(&new_page);
            }
        }
        return NULL;
    }
}

void set_empty_page(struct database *db, struct page *empty_page) {
    assert(db != NULL);
    if (empty_page != NULL) {
        struct page *prev_page = get_page(db, empty_page->header.prev_page);
        struct page *next_page = get_page(db, empty_page->header.next_page);
        if (prev_page != NULL && next_page != NULL) {
            prev_page->header.next_page = next_page->addr;
            next_page->header.prev_page = prev_page->addr;
        } else if (prev_page != NULL) {
            prev_page->header.next_page = 0;
        } else if (next_page != NULL) {
            next_page->header.prev_page = 0;
        }
        stop_using_page(db, &prev_page);
        stop_using_page(db, &next_page);
        empty_page->header.end_page = sizeof(struct file_page_header);
        empty_page->header.prev_page = 0;
        empty_page->header.next_page = 0;
        if (db->header->empty_pages > 0) {
            empty_page->header.next_page = db->header->empty_pages;
        }
        db->header->empty_pages = empty_page->addr;
    }
}