//
// Created by gav66 on 10/16/2023.
//
#include <stdlib.h>
#include <assert.h>
#include <memory.h>
#include "../page/page_handler.h"
#include "page_chain_handler.h"

static int64_t reserve_block(struct database *db, int64_t addr, uint64_t size) {
    assert(db != NULL);
    int64_t page_offset = addr % PAGE_SIZE;
    if (page_offset >= sizeof(struct file_page_header)) {
        struct page *page = get_page(db, addr);
        if (page != NULL) {
            uint64_t unreserved_mem = size;
            while (PAGE_SIZE - page_offset < unreserved_mem) {
                unreserved_mem -= PAGE_SIZE - page_offset;
                if (page->header.next_page > 0) {
                    int64_t next_addr = page->header.next_page;
                    stop_using_page(db, &page);
                    page = get_page(db, next_addr);
                } else {
                    struct page *empty_page = get_empty_page(db);
                    if (empty_page != NULL) {
                        link_pages(page, empty_page);
                        stop_using_page(db, &page);
                        page = empty_page;
                    } else {
                        stop_using_page(db, &page);
                        return 0;
                    }
                }
                page_offset = sizeof(struct file_page_header);
            }
            int64_t last_page_addr = page->addr;
            stop_using_page(db, &page);
            return last_page_addr;
        }
    }
    return 0;
}

enum block_status
save_block(struct database *db, struct page_chain_header *chain_header, int64_t addr, struct buffer *block) {
    assert(db != NULL);
    if (block != NULL) {
        struct page *first_page = get_page(db, addr);
        if (first_page != NULL) {
            int64_t block_size = chain_header->block_size;
            if (block_size >= block->size) {
                int64_t page_offset = addr % PAGE_SIZE;
                if (page_offset >= sizeof(struct file_page_header)) {
                    if (PAGE_SIZE - page_offset >= block_size) {
                        if (page_offset + block_size > first_page->header.end_page) {
                            first_page->header.end_page = page_offset + block_size;
                        }
                        memcpy(first_page->data + page_offset, block->data, block_size);
                    } else {
                        int64_t last_page_addr = reserve_block(db, addr, block_size);
                        if (last_page_addr > 0) {
                            int64_t unsaved_data = block_size;
                            int64_t block_offset = 0;
                            int64_t cur_addr = first_page->addr;
                            while (unsaved_data > 0) {
                                struct page *page = get_page(db, cur_addr);
                                if (page != NULL) {
                                    if (PAGE_SIZE - page_offset >= unsaved_data) {
                                        if (page_offset + unsaved_data > page->header.end_page) {
                                            page->header.end_page = page_offset + unsaved_data;
                                        }
                                        memcpy(page->data + page_offset, block->data + block_offset, unsaved_data);
                                        unsaved_data = 0;
                                    } else {
                                        page->header.end_page = PAGE_SIZE;
                                        memcpy(page->data + page_offset, block->data + block_offset,
                                               PAGE_SIZE - page_offset);
                                        unsaved_data -= PAGE_SIZE - page_offset;
                                        block_offset += PAGE_SIZE - page_offset;
                                        page_offset = sizeof(struct file_page_header);
                                        cur_addr = page->header.next_page;
                                    }
                                    stop_using_page(db, &page);
                                } else {
                                    stop_using_page(db, &first_page);
                                    return BLOCK_SAVE_ERROR;
                                }
                            }
                            chain_header->last_page_addr = last_page_addr;
                        } else {
                            stop_using_page(db, &first_page);
                            return BLOCK_SAVE_ERROR;
                        }
                    }
                }
            } else {
                stop_using_page(db, &first_page);
                return BLOCK_SAVE_ERROR;
            }
            stop_using_page(db, &first_page);
            return BLOCK_SUCCESS;
        }
    }
    return BLOCK_SAVE_ERROR;
}

struct buffer *read_block(struct database *db, struct page_chain_header *chain_header, int64_t addr) {
    assert(db != NULL);
    int64_t page_offset = addr % PAGE_SIZE;
    if (page_offset >= sizeof(struct file_page_header)) {
        int64_t block_size = chain_header->block_size;
        struct buffer *block = buffer_init(block_size);
        if (block != NULL) {
            int64_t unread_data = block_size;
            int64_t block_offset = 0;
            int64_t cur_addr = addr;
            while (unread_data > 0) {
                struct page *page = get_page(db, cur_addr);
                if (page != NULL) {
                    if (PAGE_SIZE - page_offset >= unread_data) {
                        memcpy(block->data + block_offset, page->data + page_offset, unread_data);
                        unread_data = 0;
                    } else {
                        memcpy(block->data + block_offset, page->data + page_offset, PAGE_SIZE - page_offset);
                        unread_data -= PAGE_SIZE - page_offset;
                        block_offset += PAGE_SIZE - page_offset;
                        page_offset = sizeof(struct file_page_header);
                        if (page->header.next_page == 0) {
                            stop_using_page(db, &page);
                            buffer_free(&block);
                            return NULL;
                        }
                        cur_addr = page->header.next_page;
                    }
                    stop_using_page(db, &page);
                } else {
                    buffer_free(&block);
                    return NULL;
                }
            }
            block->wcurs = block_size;
            return block;
        }
    }
    return NULL;
}

//Адрес должен указывать на следующий бит после конца блока
struct buffer *read_block_from_end(struct database *db, struct page_chain_header *chain_header, int64_t addr) {
    assert(db != NULL);
    int64_t page_offset = addr % PAGE_SIZE;
    if (page_offset == 0) {
        page_offset = PAGE_SIZE;
        addr -= PAGE_SIZE;
    }
    if (page_offset > sizeof(struct file_page_header)) {
        int64_t block_size = chain_header->block_size;
        struct buffer *block = buffer_init(block_size);
        if (block != NULL) {
            int64_t unread_data = block_size;
            int64_t block_offset = block_size;
            int64_t cur_addr = addr;
            while (unread_data > 0) {
                struct page *page = get_page(db, cur_addr);
                if (page != NULL) {
                    if (page_offset - sizeof(struct file_page_header) >= unread_data) {
                        memcpy(block->data, page->data + page_offset - unread_data, unread_data);
                        unread_data = 0;
                    } else {
                        block_offset -= (int64_t) (page_offset - sizeof(struct file_page_header));
                        memcpy(block->data + block_offset,
                               page->data + sizeof(struct file_page_header),
                               page_offset - sizeof(struct file_page_header));
                        unread_data -= (int64_t) (page_offset - sizeof(struct file_page_header));
                        page_offset = PAGE_SIZE;
                        if (page->header.prev_page <= 0) {
                            stop_using_page(db, &page);
                            buffer_free(&block);
                            return NULL;
                        }
                        cur_addr = page->header.prev_page;
                    }
                    stop_using_page(db, &page);
                } else {
                    buffer_free(&block);
                    return NULL;
                }
            }
            block->wcurs = block_size;
            return block;
        }
    }
    return NULL;
}

enum block_status remove_block(struct database *db, struct page_chain_header *chain_header, int64_t addr) {
    assert(db != NULL);
    int64_t page_offset = addr % PAGE_SIZE;
    if (page_offset >= sizeof(struct file_page_header)) {
        struct page *first_page = get_page(db, addr);
        if (first_page != NULL) {
            int64_t block_size = chain_header->block_size;
            int64_t unread_mem = block_size;
            int64_t cur_addr = addr;
            int64_t pointer = page_offset;
            struct page *page = NULL;
            while (unread_mem > 0) {
                page = get_page(db, cur_addr);
                if (page != NULL) {
                    if (PAGE_SIZE - pointer >= unread_mem) {
                        break;
                    } else {
                        unread_mem -= PAGE_SIZE - pointer;
                        pointer = sizeof(struct file_page_header);
                        if (page->header.next_page == 0) {
                            stop_using_page(db, &first_page);
                            stop_using_page(db, &page);
                            return BLOCK_REMOVE_ERROR;
                        }
                        cur_addr = page->header.next_page;
                        stop_using_page(db, &page);
                    }
                } else {
                    stop_using_page(db, &first_page);
                    return BLOCK_REMOVE_ERROR;
                }
            }
            if (page != NULL) {
                if (pointer + unread_mem == page->header.end_page &&
                    page->header.next_page == 0) {
                    while (page != first_page) {
                        int64_t prev_page_addr = page->header.prev_page;
                        set_empty_page(db, page);
                        stop_using_page(db, &page);
                        page = get_page(db, prev_page_addr);
                        if (page == NULL) {
                            stop_using_page(db, &first_page);
                            return BLOCK_REMOVE_ERROR;
                        }
                    }
                    chain_header->last_page_addr = page->addr;
                    page->header.end_page = page_offset;
                    if (page->header.end_page == sizeof(struct file_page_header)) {
                        chain_header->last_page_addr = page->header.prev_page;
                        set_empty_page(db, page);
                    }
                    if (chain_header->last_page_addr <= 0) {
                        chain_header->first_page_addr = 0;
                    }
                    stop_using_page(db, &first_page);
                    stop_using_page(db, &page);
                    return BLOCK_SUCCESS;
                } else {
                    stop_using_page(db, &page);
                    int64_t last_page_addr = chain_header->last_page_addr;
                    struct page *last_page = get_page(db, last_page_addr);
                    if (last_page != NULL) {
                        struct buffer *block = read_block_from_end(db, chain_header,
                                                                   last_page->addr + last_page->header.end_page);
                        if (save_block(db, chain_header, addr, block) == BLOCK_SUCCESS) {
                            buffer_free(&block);
                            cur_addr = last_page->addr;
                            pointer = last_page->header.end_page;
                            stop_using_page(db, &last_page);
                            int64_t unreleased_data = block_size;
                            while (unreleased_data > 0) {
                                page = get_page(db, cur_addr);
                                if (page != NULL) {
                                    if (pointer - sizeof(struct file_page_header) >= unreleased_data) {
                                        break;
                                    } else {
                                        unreleased_data -= (int64_t) (pointer - sizeof(struct file_page_header));
                                        pointer = PAGE_SIZE;
                                        if (page->header.prev_page <= 0) {
                                            stop_using_page(db, &first_page);
                                            stop_using_page(db, &page);
                                            return BLOCK_REMOVE_ERROR;
                                        }
                                        cur_addr = page->header.prev_page;
                                        set_empty_page(db, page);
                                        stop_using_page(db, &page);
                                    }
                                } else {
                                    stop_using_page(db, &first_page);
                                    return BLOCK_REMOVE_ERROR;
                                }
                            }
                            chain_header->last_page_addr = page->addr;
                            page->header.end_page -= unreleased_data;
                            if (page->header.end_page == sizeof(struct file_page_header)) {
                                chain_header->last_page_addr = page->header.prev_page;
                                set_empty_page(db, page);
                            }
                            if (chain_header->last_page_addr <= 0) {
                                chain_header->first_page_addr = 0;
                            }
                            stop_using_page(db, &first_page);
                            stop_using_page(db, &page);
                            return BLOCK_SUCCESS;
                        }
                    }
                }
            }
        }
    }
    return BLOCK_REMOVE_ERROR;
}

enum chain_status page_chain_clear(struct database *db, struct page_chain_header *chain_header) {
    assert(db != NULL && chain_header != NULL);
    while (chain_header->last_page_addr != 0) {
        struct page *chain_page = get_page(db, chain_header->last_page_addr);
        if (chain_page != NULL) {
            chain_header->last_page_addr = chain_page->header.prev_page;
            set_empty_page(db, chain_page);
            stop_using_page(db, &chain_page);
        } else {
            return CHAIN_CLEAR_ERROR;
        }
    }
    chain_header->first_page_addr = 0;
    return CHAIN_SUCCESS;
}

enum chain_status
page_chain_append(struct database *db, struct page_chain_header *chain_header, struct buffer *buffer) {
    assert(db != NULL && chain_header != NULL && buffer != NULL);
    struct page *last_page = NULL;
    if (chain_header->last_page_addr != 0) {
        last_page = get_page(db, chain_header->last_page_addr);
    } else {
        last_page = get_empty_page(db);
        if (last_page != NULL) {
            chain_header->first_page_addr = last_page->addr;
            chain_header->last_page_addr = last_page->addr;
        }
    }
    if (last_page != NULL) {
        if (last_page->header.end_page == PAGE_SIZE) {
            int64_t addr = reserve_block(db,
                                         last_page->addr + last_page->header.end_page - 1,
                                         chain_header->block_size + 1
            );
            if (addr != 0) {
                if (save_block(db,
                               chain_header,
                               (int64_t) (last_page->addr + PAGE_SIZE + sizeof(struct file_page_header)),
                               buffer) != BLOCK_SUCCESS) {
                    stop_using_page(db, &last_page);
                    return CHAIN_APPEND_ERROR;
                }
                stop_using_page(db, &last_page);
                chain_header->last_page_addr = addr;
                return CHAIN_SUCCESS;
            }
        } else {
            if (save_block(db, chain_header, last_page->addr + last_page->header.end_page, buffer) != BLOCK_SUCCESS) {
                stop_using_page(db, &last_page);
                return CHAIN_APPEND_ERROR;
            }
            stop_using_page(db, &last_page);
            return CHAIN_SUCCESS;
        }
        stop_using_page(db, &last_page);
    }
    return CHAIN_APPEND_ERROR;

}

bool page_chain_is_empty(struct page_chain_header *header) {
    return header->first_page_addr == 0;
}

struct chain_iter *page_chain_iterator(struct database *db, struct page_chain_header *chain_header) {
    assert(db != NULL && chain_header != NULL);
    struct chain_iter *iter = malloc(sizeof(struct chain_iter));
    if (iter != NULL) {
        if (!page_chain_is_empty(chain_header)) {
            struct page *page = get_page(db, chain_header->first_page_addr);
            if (page != NULL) {
                iter->page = page;
                iter->offset = sizeof(struct file_page_header);
                iter->chain_header = chain_header;
                return iter;
            } else {
                free(iter);
                return NULL;
            }
        }
        iter->page = NULL;
        iter->offset = 0;
        iter->chain_header = chain_header;
        return iter;
    }
    return NULL;
}

void page_chain_iterator_free(struct database *db, struct chain_iter **iterator) {
    assert(db != NULL && iterator != NULL);
    struct chain_iter *iter = *iterator;
    if (iter == NULL) {
        return;
    }
    stop_using_page(db, &iter->page);
    free(iter);
    *iterator = NULL;
}

enum chain_status page_chain_iterator_delete(struct database *db, struct chain_iter *iter) {
    assert(db != NULL && iter != NULL);
    if (remove_block(db, iter->chain_header, iter->page->addr + iter->offset) != BLOCK_SUCCESS) {
        return CHAIN_DELETE_ERROR;
    }
    return CHAIN_SUCCESS;
}

struct buffer *page_chain_iterator_get(struct database *db, struct chain_iter *iter) {
    assert(db != NULL && iter != NULL);
    if (iter->offset + iter->chain_header->block_size > iter->page->header.end_page
        && iter->page->header.end_page < PAGE_SIZE) {
        return NULL;
    }
    if (iter->offset == PAGE_SIZE) {
        return read_block(db, iter->chain_header,
                          (int64_t) (iter->page->addr + PAGE_SIZE + sizeof(struct file_page_header)));
    }
    return read_block(db, iter->chain_header, iter->page->addr + iter->offset);
}

enum chain_status page_chain_iterator_next(struct database *db, struct chain_iter *iter) {
    assert(db != NULL && iter != NULL);
    int64_t block_size = iter->chain_header->block_size;
    int64_t unread_mem = block_size;
    int64_t cur_addr = iter->page->addr;
    int64_t pointer = iter->offset;
    struct page *page = NULL;
    while (unread_mem > 0) {
        page = get_page(db, cur_addr);
        if (page != NULL) {
            if (PAGE_SIZE - pointer >= unread_mem) {
                break;
            } else {
                unread_mem -= PAGE_SIZE - pointer;
                pointer = sizeof(struct file_page_header);
                if (page->header.next_page == 0) {
                    stop_using_page(db, &page);
                    return CHAIN_ITER_ERROR;
                }
                cur_addr = page->header.next_page;
                stop_using_page(db, &page);
            }
        } else {
            return CHAIN_ITER_ERROR;
        }
    }
    if (page != NULL) {
        if (pointer + unread_mem >= page->header.end_page &&
            page->header.next_page == 0) {
            stop_using_page(db, &page);
            return CHAIN_ITER_END;
        } else {
            stop_using_page(db, &iter->page);
            iter->page = page;
            iter->offset = pointer + unread_mem;
            return CHAIN_SUCCESS;
        }
    }
    return CHAIN_ITER_ERROR;
}


