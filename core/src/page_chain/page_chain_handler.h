//
// Created by gav66 on 10/16/2023.
//

#ifndef LLP_PAGE_CHAIN_HANDLER_H
#define LLP_PAGE_CHAIN_HANDLER_H
#include "../database/database.h"
#include "../utils/buffer/buffer.h"

enum block_status {
    BLOCK_SUCCESS = 0,
    BLOCK_SAVE_ERROR,
    BLOCK_READ_ERROR,
    BLOCK_REMOVE_ERROR,
};

enum chain_status{
    CHAIN_SUCCESS = 0,
    CHAIN_CLEAR_ERROR,
    CHAIN_DELETE_ERROR,
    CHAIN_ITER_ERROR,
    CHAIN_ITER_END,
    CHAIN_APPEND_ERROR
};

struct page_chain_header {
    int64_t block_size;
    int64_t first_page_addr;
    int64_t last_page_addr;
};

struct chain_iter {
    struct page_chain_header* chain_header;
    struct page* page;
    int64_t offset;
};

enum block_status save_block(struct database *db, struct page_chain_header *chain_header, int64_t addr, struct buffer *block);
struct buffer *read_block(struct database *db, struct page_chain_header *chain_header, int64_t addr);
struct buffer *read_block_from_end(struct database *db, struct page_chain_header *chain_header, int64_t addr);
enum block_status remove_block(struct database *db, struct page_chain_header *chain_header, int64_t addr);
enum chain_status page_chain_clear(struct database* db, struct page_chain_header* chain_header);
bool page_chain_is_empty(struct page_chain_header *header);
struct chain_iter *page_chain_iterator(struct database *db, struct page_chain_header *chain_header);
void page_chain_iterator_free(struct database *db, struct chain_iter **iterator);
enum chain_status page_chain_iterator_delete(struct database *db, struct chain_iter *iter);
struct buffer *page_chain_iterator_get(struct database *db, struct chain_iter *iter);
enum chain_status page_chain_iterator_next(struct database *db, struct chain_iter *iter);
enum chain_status page_chain_append(struct database *db, struct page_chain_header *chain_header, struct buffer *buffer);

#endif //LLP_PAGE_CHAIN_HANDLER_H
