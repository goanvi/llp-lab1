//
// Created by gav66 on 11/1/2023.
//
#include <stdlib.h>
#include <assert.h>
#include "table_builder.h"

struct table_builder *table_builder_init(char *table_name) {
    assert(table_name != NULL);
    struct table_builder *table = malloc(sizeof(struct table_builder));
    if (table != NULL) {
        table->table_name = table_name;
        table->column_list = NULL;
        table->column_num = 0;
        return table;
    }
    return NULL;
}

void table_builder_free(struct table_builder **table) {
    assert(table != NULL);
    if (*table == NULL) {
        return;
    }
    struct table_builder_column *column = (*table)->column_list;
    while (column != NULL) {
        struct table_builder_column *next_col = column->next_col;
        free(column);
        column = next_col;
    }
    free(*table);
    *table = NULL;
}

void table_builder_add_column(struct table_builder *table, char *col_name, enum data_type col_type) {
    assert(table != NULL && col_name != NULL);
    struct table_builder_column *column = malloc(sizeof(struct table_builder_column));
    if (column != NULL) {
        column->column_name = col_name;
        column->column_type = col_type;
        column->next_col = NULL;
        struct table_builder_column *list = table->column_list;
        if (list == NULL) {
            table->column_list = column;
            table->column_num++;
            return;
        }
        while (list->next_col != NULL) {
            list = list->next_col;
        }
        list->next_col = column;
        table->column_num++;
    }
}


