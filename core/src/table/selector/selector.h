//
// Created by gav66 on 11/28/2023.
//

#ifndef LLP_SELECTOR_H
#define LLP_SELECTOR_H

struct column_description_list {
    struct column_description *column;
    struct column_description_list *next;
};

struct selector_builder {
    struct column_description_list *columns_list;
    size_t size;
};

struct selector_builder *selector_builder_init();

void selector_builder_free(struct selector_builder **selector);

void selector_builder_add(struct selector_builder *selector, char *table_name, char *column_name);

#endif //LLP_SELECTOR_H
