//
// Created by gav66 on 11/28/2023.
//

#include <assert.h>
#include <stdlib.h>
#include "selector.h"
#include "../table.h"

struct selector_builder *selector_builder_init() {
    struct selector_builder *selector = malloc(sizeof(struct selector_builder));
    selector->columns_list = NULL;
    selector->size = 0;
    return selector;
}

void selector_builder_free(struct selector_builder **selector_ptr) {
    assert(selector_ptr != NULL);
    struct selector_builder *selector = *selector_ptr;
    if (NULL == selector) {
        return;
    }
    while (selector->columns_list != NULL){
        struct column_description_list *next = selector->columns_list->next;
        free(selector->columns_list->column);
        free(selector->columns_list);
        selector->columns_list = next;
    }
    free(selector);
    *selector_ptr = NULL;
}

void selector_builder_add(struct selector_builder *selector, char *table_name, char *column_name) {
    struct column_description *column = malloc(sizeof(struct column_description));
    column->type = COLUMN_DESC_NAME;
    column->name.table_name = table_name;
    column->name.column_name = column_name;
    struct column_description_list *list = malloc(sizeof(struct column_description_list));
    list->column = column;
    list->next = NULL;
    if (selector->columns_list != NULL) {
        struct column_description_list *desc_list = selector->columns_list;
        while (desc_list->next != NULL){
            desc_list = desc_list->next;
        }
        desc_list->next = list;
    }else{
        selector->columns_list = list;
    }
    selector->size++;
}
