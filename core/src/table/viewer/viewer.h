//
// Created by gav66 on 11/23/2023.
//

#ifndef LLP_VIEWER_H
#define LLP_VIEWER_H

#include "../table.h"
#include "../cursor/cursor.h"

struct result_view {
    struct table_struct *view_scheme;
    struct column_description *view_selector;
    struct cursor *cursor;
};

void result_view_free(struct result_view **view_ptr);

bool result_view_is_empty(struct result_view *view);

struct row result_view_get(struct result_view *view);

void result_view_next(struct result_view *view);

struct table_struct *result_view_scheme(struct result_view *view);

#endif //LLP_VIEWER_H
