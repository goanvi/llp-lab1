//
// Created by gav66 on 11/8/2023.
//

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "cursor/cursor.h"
#include "table_client.h"
#include "condition/condition.h"
#include "../utils/pool/pool.h"

static struct cursor *database_build_base_cursor(struct database *db, char *table_name, size_t table_idx) {
    struct cursor *cur = NULL;
    struct table *table = find_table(db, table_name);
    if (table != NULL) {
        cur = cursor_init_from(table, table_idx);
    }
    return cur;
}

static struct column_description index_column(struct table_index_list *list, struct column_description column) {
    assert(list != NULL && column.type == COLUMN_DESC_NAME);
    struct column_description result = {0};
    struct table_index_list *table_list_inst = list;
    while (table_list_inst != NULL) {
        if (strcmp(table_list_inst->table_name, column.name.table_name) == 0) {
            struct column_index_list *column_list_inst = table_list_inst->col_list;
            while (column_list_inst != NULL) {
                if (strcmp(column_list_inst->col_name, column.name.column_name) == 0) {
                    result.type = COLUMN_DESC_INDEX;
                    result.index.column_idx = column_list_inst->col_index;
                    result.index.table_idx = table_list_inst->table_index;
                    return result;
                }
                column_list_inst = column_list_inst->prev;
            }
            return result;
        }
        table_list_inst = table_list_inst->prev;
    }
    return result;
}

static struct operand column_operand_translate(struct operand op, struct table_index_list *list) {
    switch (op.type) {
        case OPERAND_VALUE_LITERAL: {
            return op;
        }
        case OPERAND_VALUE_COLUMN: {
            return (struct operand) {
                    .type = OPERAND_VALUE_COLUMN,
                    .column = index_column(list, op.column)
            };
        }
    }
}

static struct where_condition *
where_condition_translate_indexed(struct where_condition *condition, struct table_index_list *list) {
    assert(condition->type == CONDITION_COMPARE);
    struct where_condition *result = malloc(sizeof(struct where_condition));
    result->type = CONDITION_COMPARE;
    result->compare.type = condition->compare.type;
    result->compare.first = column_operand_translate(condition->compare.first, list);
    result->compare.second = column_operand_translate(condition->compare.second, list);
    return result;
}

struct where_condition *where_condition_translate(struct where_condition *condition, struct table_index_list *list) {
    switch (condition->type) {
        case CONDITION_AND:
            return where_condition_and(
                    where_condition_translate(condition->and.first, list),
                    where_condition_translate(condition->and.second, list)
            );
        case CONDITION_OR:
            return where_condition_or(
                    where_condition_translate(condition->or.first, list),
                    where_condition_translate(condition->or.second, list)
            );
        case CONDITION_NOT:
            return where_condition_not(
                    where_condition_translate(condition->not.first, list)
            );
        case CONDITION_COMPARE:
            return where_condition_translate_indexed(condition, list);
    }
}


enum table_status create_table(struct database *db, struct table_builder *table_sample) {
    assert(db != NULL && table_sample != NULL);
    struct table_struct *table = table_struct_create(table_sample);
    if (table != NULL) {
        if (find_table_struct(db, table->name) == NULL) {
            uint64_t pool_addr = pool_create(db);
            if (pool_addr != 0) {
                table->pool_addr = pool_addr;
                struct buffer *ser_table = table_struct_serialize(table);
                if (ser_table != NULL) {
                    if (pool_append(db->table_pool, ser_table) == POOL_SUCCESS) {
                        return TABLE_SUCCESS;
                    }
                }
            }
        } else {
            return TABLE_ALREADY_EXIST;
        }
    }
    return TABLE_CREATE_ERROR;
}

enum table_status delete_table(struct database *db, char *name) {
    struct buffer *buffer = NULL;
    struct table_struct *schema = NULL;
    struct pool_iter *it = pool_iterator(db->table_pool);
    while (!pool_iterator_is_empty(it)) {
        buffer = pool_iterator_get(it);
        assert(buffer != NULL);
        schema = table_struct_deserialize(buffer);
        if (strcmp(schema->name, name) == 0) {
            struct pool *pool = pool_init(db, (int64_t) schema->pool_addr);
            if (pool != NULL) {
                pool_drop(&pool);
                schema->pool_addr = 0;
                pool_iterator_delete(it);
                break;
            }
            return TABLE_DELETE_ERROR;
        }
        table_struct_free(&schema);
        buffer_free(&buffer);
        pool_iterator_next(it);
    }
    table_struct_free(&schema);
    buffer_free(&buffer);
    pool_iterator_free(&it);
    return TABLE_SUCCESS;
}

static struct cursor *query_cursor(struct database *db, struct query query, struct table_index_list *tables) {
    assert(db != NULL && query.table != NULL);
    struct cursor *result = NULL;
    result = database_build_base_cursor(db, query.table, 0);
    if (result != NULL) {
        if (query.joins != NULL) {
            struct table_index_list *t_list = tables->prev;
            struct join_condition_list *joins = query.joins;
            while (joins != NULL) {
                struct cursor *right = database_build_base_cursor(db, t_list->table_name, t_list->table_index);
                if (right != NULL) {
                    struct join_condition_list index_condition;
                    index_condition.left = index_column(tables, joins->left);
                    index_condition.right = index_column(tables, joins->right);
                    index_condition.next = NULL;
                    struct cursor *joined = cursor_init_join(result, right, index_condition);
                    if (joined != NULL) {
                        result = joined;
                        joins = joins->next;
                        t_list = t_list->prev;
                    } else {
                        cursor_free(&right);
                        cursor_free(&result);
                        return NULL;
                    }
                } else {
                    cursor_free(&result);
                    return NULL;
                }
            }
        }
        if (query.where != NULL) {
            struct where_condition *translated_condition = where_condition_translate(query.where, tables);
            if (translated_condition != NULL) {
                struct cursor *tmp = cursor_init_where(result, translated_condition);
                if (tmp != NULL) {
                    result = tmp;
                    return result;
                } else {
                    where_condition_free(translated_condition);
                }
            }
            cursor_free(&result);
            return NULL;
        }
    }
    return result;
}

static struct table_index_list *index_table(struct database *db, char *table_name, size_t table_idx) {
    assert(table_name != NULL);
    struct table_struct *table = find_table_struct(db, table_name);
    if (table != NULL) {
        struct table_index_list *table_list = malloc(sizeof(struct table_index_list));
        if (table_list != NULL) {
            table_list->table_name = table_name;
            table_list->table_index = table_idx;
            struct column_index_list *column = NULL;
            for (int32_t i = (int32_t) table->col_num - 1; i >= 0; i--) {
                struct column_index_list *col = malloc(sizeof(struct column_index_list));
                if (col != NULL) {
                    col->col_type = (enum data_type) table->columns[i].col_type;
                    col->col_name = table->columns[i].name;
                    col->col_index = i;
                    col->prev = column;
                } else {
                    free(table_list);
                    return NULL;
                }
                column = col;
            }
            table_list->col_list = column;
            table_list->prev = NULL;
            return table_list;
        }
    }
    return NULL;
}

static void table_index_list_free(struct table_index_list **list_p) {
    assert(list_p != NULL);
    struct table_index_list *list = *list_p;
    if (list == NULL) {
        return;
    }
    while (list != NULL) {
        struct table_index_list *next = list->prev;
        while (list->col_list != NULL) {
            struct column_index_list *col_next = list->col_list->prev;
            free(list->col_list);
            list->col_list = col_next;
        }
        free(list);
        list = next;
    }
}

static struct table_index_list *index_query(struct database *db, struct query query) {
    assert(db != NULL);
    size_t count = 0;
    struct table_index_list *t_list = index_table(db, query.table, count);
    struct join_condition_list *j_list = query.joins;
    while (j_list != NULL) {
        struct table_index_list *subt_list = t_list;
        while (subt_list != NULL) {
            if (strcmp(j_list->left.name.table_name, subt_list->table_name) == 0) {
                struct table_index_list *idx_table = index_table(db, j_list->right.name.table_name, count + 1);
                if (idx_table != NULL) {
                    count++;
                    while (subt_list->prev != NULL) {
                        subt_list = subt_list->prev;
                    }
                    subt_list->prev = idx_table;
                }
                break;
            }
            subt_list = subt_list->prev;
        }
        j_list = j_list->next;
    }
    return t_list;
}


static bool row_is_valid(struct table_struct *scheme, struct row_builder row) {
    for (size_t i = 0; i < row.size; i++) {
        if (scheme->columns[i].col_type != (uint8_t) row.columns[i].type) {
            return false;
        }
    }
    return true;
}

static bool batch_is_valid(struct table_struct *scheme, struct batch_builder batch) {
    for (size_t i = 0; i < batch.size; i++) {
        if (!row_is_valid(scheme, batch.rows[i])) {
            return false;
        }
    }
    return true;
}

enum table_status table_insert(struct database *db, char *name, struct batch_builder batch) {
    assert(db != NULL && name != NULL);
    struct buffer *buffer = NULL;
    struct table *table = find_table(db, name);
    if (table != NULL) {
        if (batch_is_valid(table->table_struct, batch)) {
            for (size_t i = 0; i < batch.size; i++) {
                struct row row = row_builder_as_row(&(batch.rows[i]));
                buffer = row_serialize(row);
                if (pool_append(table->table_pool, buffer) != POOL_SUCCESS) {
                    buffer_free(&buffer);
                    return TABLE_INSERT_ERROR;
                }
                buffer_free(&buffer);
            }
            table_free(&table);
            return TABLE_SUCCESS;
        }

    }
    return TABLE_INSERT_ERROR;
}

static struct table_struct *
create_view_scheme(struct database *db, struct selector_builder *selector, struct table_index_list *list) {
    struct table_builder *view_scheme_builder = table_builder_init("");
    struct column_description_list *column_description_list = selector->columns_list;
    while (column_description_list != NULL) {
        struct column_description *description = column_description_list->column;
        assert(description != NULL);
        struct column_description indexed_description = index_column(list, *description);
        struct table_struct *scheme = find_table_struct(db, description->name.table_name);
        struct table_column col = scheme->columns[indexed_description.index.column_idx];
        table_builder_add_column(view_scheme_builder, col.name, col.col_type);
        table_struct_free(&scheme);
        column_description_list = column_description_list->next;
    }
    struct table_struct *result = table_struct_create(view_scheme_builder);
    table_builder_free(&view_scheme_builder);
    return result;
}

static struct column_description *
create_view_selector(struct selector_builder *selector, struct table_index_list *list) {
    struct column_description *result = malloc(sizeof(struct column_description) * selector->size);
    int i = 0;
    struct column_description_list *column_description_list = selector->columns_list;
    while (column_description_list != NULL) {
        struct column_description *description = column_description_list->column;
        assert(description != NULL);
        result[i] = index_column(list, *description);
        i++;
        column_description_list = column_description_list->next;
    }
    return result;
}

struct result_view *table_select(struct database *db, struct query query, struct selector_builder *selector) {
    assert(db != NULL);
    struct result_view *result = NULL;
    struct cursor *cur = NULL;
    struct table_index_list *index_list = NULL;
    result = malloc(sizeof(struct result_view));
    if (result != NULL) {
        index_list = index_query(db, query);
        if (index_list != NULL) {
            cur = query_cursor(db, query, index_list);
            if (cur != NULL) {
                result->cursor = cur;
                result->view_scheme = create_view_scheme(db, selector, index_list);
                result->view_selector = create_view_selector(selector, index_list);
                table_index_list_free(&index_list);
                return result;
            }
            table_index_list_free(&index_list);
        }
        free(result);
    }
    return NULL;
}

enum table_status table_delete(struct database *db, struct query query) {
    assert(db != NULL && query.joins == NULL);
    struct cursor *cur = NULL;
    struct table_index_list *index_list = NULL;
    index_list = index_query(db, query);
    if (index_list != NULL) {
        cur = query_cursor(db, query, index_list);
        if (cur != NULL) {
            while (!cursor_is_empty(cur)) {
                cursor_delete(cur, 0);
                cursor_next(cur);
            }
            cursor_free(&cur);
            table_index_list_free(&index_list);
            return TABLE_SUCCESS;
        }
        table_index_list_free(&index_list);
    }
    return TABLE_DELETE_ERROR;
}

enum table_status table_update(struct database *db, struct query query, struct updater_builder *updater) {
    assert(db != NULL && query.joins == NULL);
    struct cursor *cur = NULL;
    struct table_index_list *index_list = NULL;
    struct updater_builder *translated_updater = NULL;
    index_list = index_query(db, query);
    if (index_list != NULL) {
        cur = query_cursor(db, query, index_list);
        if (cur != NULL) {
            translated_updater = updater_builder_translate(updater, index_list);
            if (translated_updater != NULL) {
                struct buffer_list *list = NULL;
                struct cursor *cur_from = NULL;
                if (cur->type == CURSOR_WHERE){
                    cur_from = cur->where.base;
                }else{
                    cur_from = cur;
                }
                while (!cursor_is_empty(cur)) {
                    struct buffer *buffer = NULL;
                    buffer = pool_iterator_get(cur_from->from.it);
                    if (buffer != NULL){
                        struct row row = row_deserialize(cur_from->from.table->table_struct, buffer);
                        buffer_free(&buffer);
                        struct row updated_row = updater_builder_update(translated_updater, row);
                        struct buffer *serialized = row_serialize(updated_row);
                        struct buffer_list *list_inst = malloc(sizeof(struct buffer_list));
                        row_free(updated_row);
                        row_free(row);
                        list_inst->buffer = serialized;
                        list_inst->next = NULL;
                        if (list != NULL){
                            list_inst->next = list;
                        }
                        list = list_inst;
                    }
                    cursor_delete(cur, 0);
                    cursor_next(cur);
                }
                while(list != NULL){
                    struct buffer_list *next = list->next;
                    pool_append(cur_from->from.table->table_pool, list->buffer);
                    buffer_free(&list->buffer);
                    free(list);
                    list = next;
                }
                cursor_free(&cur);
                table_index_list_free(&index_list);
                updater_builder_free(&translated_updater);
                return TABLE_SUCCESS;
            }
            cursor_free(&cur);
        }
        table_index_list_free(&index_list);
    }
    return TABLE_UPDATE_ERROR;
}
