//
// Created by gav66 on 11/29/2023.
//

#ifndef LLP_UPDATER_H
#define LLP_UPDATER_H

#include "../table.h"

struct column_updater {
    bool translated;
    union {
        size_t idx;
        char *name;
    } target;
    struct row_column new_value;
};

struct column_updater_list {
    struct column_updater *updater;
    struct column_updater_list *next;
};

struct updater_builder {
    struct column_updater_list *list;
};

struct updater_builder *
updater_builder_translate(struct updater_builder *old_updater, struct table_index_list *index_list);

struct row updater_builder_update(struct updater_builder *updater, struct row row);

void updater_builder_add(struct updater_builder *updater, struct column_updater *col_updater);

struct updater_builder *updater_builder_init();

struct column_updater *column_updater_of(char *target, struct row_column new_value);

void updater_builder_free(struct updater_builder **updater_ptr);

#endif //LLP_UPDATER_H
