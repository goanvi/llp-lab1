//
// Created by gav66 on 11/1/2023.
//

#ifndef LLP_TABLE_H
#define LLP_TABLE_H

#include <stdint.h>
#include <stdbool.h>
#include "../database/database.h"
#include "table_builder/table_builder.h"
#include "../utils/buffer/buffer.h"


enum table_status {
    TABLE_SUCCESS = 0,
    TABLE_ALREADY_EXIST,
    TABLE_CREATE_ERROR,
    TABLE_DELETE_ERROR,
    TABLE_INSERT_ERROR,
    TABLE_UPDATE_ERROR,
};

struct table_column {
    uint8_t col_type;
    char *name;
};

struct table_struct {
    uint32_t size;
    uint64_t pool_addr;
    char *name;
    uint32_t col_num;
    struct table_column *columns;
};

struct table {
    struct table_struct *table_struct;
    struct pool *table_pool;
};
union column_value {
    float val_float;
    int32_t val_int;
    uint8_t val_bool;
    char *val_string;
};

struct row_column {
    enum data_type type;
    union column_value value;
};

struct row {
    struct row_column *columns;
    size_t size;
};

enum column_description_type {
    COLUMN_DESC_NAME = 0,
    COLUMN_DESC_INDEX = 1,
};

struct column_description {
    enum column_description_type type;
    union {
        struct {
            char *table_name;
            char *column_name;
        } name;
        struct {
            size_t table_idx;
            size_t column_idx;
        } index;
    };
};

struct table_index_list {
    char *table_name;
    unsigned int table_index;
    struct column_index_list *col_list;
    struct table_index_list *prev;
};

struct column_index_list {
    char *col_name;
    unsigned int col_index;
    enum data_type col_type;
    struct column_index_list *prev;
};

enum table_status create_table(struct database *db, struct table_builder *table_sample);

struct buffer *row_serialize(struct row row);

struct row row_deserialize(struct table_struct *table, struct buffer *buffer);

void table_free(struct table **table_p);

struct table *find_table(struct database *db, const char *name);

struct table_struct *table_struct_create(struct table_builder *table_scheme);

struct table_struct *find_table_struct(struct database *db, const char *name);

struct buffer *table_struct_serialize(const struct table_struct *table_schema);

struct table_struct *table_struct_deserialize(struct buffer *buffer);

void table_struct_free(struct table_struct **table_p);

struct row_column column_int(int32_t value);

struct row_column column_float(float value);

struct row_column column_string(char *value);

struct row_column column_bool(bool value);

void row_free(struct row row);

struct column_description table_column_of(char *table_name, char *column_name);

struct row_column column_copy(struct row_column column);

struct row row_copy(struct row row);

#endif //LLP_TABLE_H
