//
// Created by gav66 on 11/1/2023.
//

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "table.h"
#include "../utils/pool/pool.h"


struct table_struct *table_struct_create(struct table_builder *table_scheme) {
    assert(table_scheme != NULL);
    struct table_struct *table = malloc(sizeof(struct table_struct));
    if (table != NULL) {
        table->pool_addr = 0;
        table->name = table_scheme->table_name;
        table->col_num = table_scheme->column_num;
        table->size = sizeof(table->pool_addr) +
                      sizeof(table->size) +
                      sizeof(table->col_num) +
                      sizeof(struct table_column) * table_scheme->column_num +
                      strlen(table_scheme->table_name) + 1;
        table->columns = malloc(sizeof(struct table_column) * table_scheme->column_num);
        if (table->columns != NULL) {
            struct table_builder_column *col = table_scheme->column_list;
            size_t iter = 0;
            while (col != NULL) {
                table->columns[iter].name = col->column_name;
                table->columns[iter].col_type = (uint8_t) col->column_type;
                iter++;
                col = col->next_col;
            }
            return table;
        }
    }
    return NULL;
}

static void free_column(struct table_column *column) {
    assert(column != NULL);
    free(column->name);
    column->name = NULL;
}

void table_struct_free(struct table_struct **table_p) {
    assert(table_p != NULL);
    struct table_struct *table = *table_p;
    if (table == NULL) {
        return;
    }
    free(table->columns);
    free(table);
    *table_p = NULL;
}

static void read_columns(struct table_struct *schema, struct buffer *buffer) {
    for (uint32_t i = 0; i < schema->col_num; i++) {
        uint8_t col_type = buffer_read_b8(buffer).ui8;
        char *col_name = buffer_read_string(buffer);
        schema->columns[i] = (struct table_column) {.col_type = col_type, .name = col_name};
    }
}

struct table_struct *table_struct_deserialize(struct buffer *buffer) {
    assert(buffer != NULL);
    buffer_reset(buffer);
    struct table_struct *schema = malloc(sizeof(struct table_struct));
    if (schema != NULL) {
        schema->size = buffer_read_b32(buffer).ui32;
        schema->pool_addr = buffer_read_b64(buffer).ui64;
        schema->name = buffer_read_string(buffer);
        schema->col_num = buffer_read_b32(buffer).ui32;
        schema->columns = malloc(sizeof(struct table_column) * schema->col_num);
        if (schema->columns != NULL) {
            read_columns(schema, buffer);
        } else {
            free(schema);
            return NULL;
        }
    }
    return schema;
}

struct buffer *table_struct_serialize(const struct table_struct *const table_schema) {
    assert(table_schema != NULL);
    struct buffer *buffer = buffer_init(table_schema->size);
    buffer_reset(buffer);
    if (buffer == NULL) {
        return NULL;
    }
    buffer_write_b32(buffer, (union u32) {.ui32 = table_schema->size});
    buffer_write_b64(buffer, (union u64) {.ui64 = table_schema->pool_addr});
    buffer_write_string(buffer, table_schema->name);
    buffer_write_b32(buffer, (union u32) {.ui32 = table_schema->col_num});
    for (uint32_t i = 0; i < table_schema->col_num; i++) {
        struct table_column col = table_schema->columns[i];
        buffer_write_b8(buffer, (union u8) {.ui8 = col.col_type});
        buffer_write_string(buffer, col.name);
    }
    return buffer;
}

struct table_struct *find_table_struct(struct database *db, const char *const name) {
    assert(db != NULL && name != NULL);
    struct buffer *buffer = NULL;
    struct table_struct *scheme = NULL;
    struct pool_iter *it = pool_iterator(db->table_pool);
    if (it != NULL) {
        while (!pool_iterator_is_empty(it)) {
            buffer = pool_iterator_get(it);
            assert(buffer != NULL);
            scheme = table_struct_deserialize(buffer);
            if (0 == strcmp(scheme->name, name)) {
                break;
            }
            table_struct_free(&scheme);
            buffer_free(&buffer);
            pool_iterator_next(it);
        }
        buffer_free(&buffer);
        pool_iterator_free(&it);
    }
    return scheme;
}

struct table *find_table(struct database *db, const char *name) {
    assert(db != NULL && name != NULL);
    struct table *table = malloc(sizeof(struct table));
    table->table_struct = find_table_struct(db, name);
    if (table->table_struct == NULL) {
        free(table);
        return NULL;
    }
    table->table_pool = pool_init(db, (int64_t) table->table_struct->pool_addr);
    if (table->table_pool == NULL) {
        table_struct_free(&table->table_struct);
        free(table);
        return NULL;
    }
    return table;
}

void table_free(struct table **table_p) {
    assert(table_p != NULL);
    struct table *table = *table_p;
    if (NULL == table) {
        return;
    }
    pool_free(&table->table_pool);
    table_struct_free(&table->table_struct);
    table->table_struct = NULL;
    free(table);
    *table_p = NULL;
}

static uint64_t row_size(struct row row) {
    assert(row.columns != NULL);
    size_t size = 0;
    for (uint32_t i = 0; i < row.size; i++) {
        struct row_column column = row.columns[i];
        switch (column.type) {
            case DATA_TYPE_INT:
                size += sizeof(union u32);
                break;
            case DATA_TYPE_FLOAT:
                size += sizeof(union u32);
                break;
            case DATA_TYPE_STRING:
                size += strlen(column.value.val_string) + 1;
                break;
            case DATA_TYPE_BOOL:
                size += sizeof(union u8);
                break;
        }
    }
    return size;
}

struct buffer *row_serialize(struct row row) {
    assert(row.columns != NULL);
    struct buffer *buffer = buffer_init(row_size(row));
    for (size_t i = 0; i < row.size; i++) {
        struct row_column col = row.columns[i];
        switch (col.type) {
            case DATA_TYPE_INT:
                buffer_write_b32(buffer, (union u32) {.i32 = col.value.val_int});
                break;
            case DATA_TYPE_FLOAT:
                buffer_write_b32(buffer, (union u32) {.f32 = col.value.val_float});
                break;
            case DATA_TYPE_STRING:
                buffer_write_string(buffer, col.value.val_string);
                break;
            case DATA_TYPE_BOOL:
                buffer_write_b8(buffer, (union u8) {.ui8 = col.value.val_bool});
                break;
        }
    }
    return buffer;
}

struct row row_deserialize(struct table_struct *table, struct buffer *buffer) {
    assert(table != NULL && buffer != NULL);
    struct row_column *columns = malloc(sizeof(struct row_column) * table->col_num);
    for (uint32_t i = 0; i < table->col_num; i++) {
        struct table_column scheme_col = table->columns[i];
        switch ((enum data_type) scheme_col.col_type) {
            case DATA_TYPE_INT:
                columns[i].value.val_int = buffer_read_b32(buffer).i32;
                break;
            case DATA_TYPE_FLOAT:
                columns[i].value.val_float = buffer_read_b32(buffer).f32;
                break;
            case DATA_TYPE_STRING: {
                columns[i].value.val_string = buffer_read_string(buffer);
                break;
            }
            case DATA_TYPE_BOOL:
                columns[i].value.val_bool = buffer_read_b8(buffer).ui8;
                break;
        }
        columns[i].type = (enum data_type) scheme_col.col_type;
    }
    return (struct row) {
            .size = table->col_num,
            .columns = columns
    };
}

struct row_column column_int(int32_t value) {
    return (struct row_column) {
            .type = DATA_TYPE_INT,
            .value = {.val_int = value}
    };
}

struct row_column column_float(float value) {
    return (struct row_column) {
            .type = DATA_TYPE_FLOAT,
            .value = {.val_float = value}
    };
}

struct row_column column_string(char *value) {
    return (struct row_column) {
            .type = DATA_TYPE_STRING,
            .value = {.val_string = value}
    };
}

struct row_column column_bool(bool value) {
    return (struct row_column) {
            .type = DATA_TYPE_BOOL,
            .value = {.val_bool = value}
    };
}

void row_free(struct row row) {
    if (NULL == row.columns) {
        return;
    }
    free(row.columns);
    row.columns = NULL;
}

struct column_description table_column_of(char *table_name, char *column_name) {
    return (struct column_description) {
            .type = COLUMN_DESC_NAME,
            .name = {
                    .table_name = table_name,
                    .column_name = column_name
            }
    };
}

struct row_column column_copy(struct row_column column) {
    struct row_column copy;
    copy.value = column.value;
    copy.type = column.type;
    return copy;
}

struct row row_copy(struct row row) {
    struct row copy;
    copy.size = row.size;
    copy.columns = malloc(sizeof(struct row_column) * copy.size);
    for (size_t i = 0; i < row.size; i++) {
        copy.columns[i] = column_copy(row.columns[i]);
    }
    return copy;
}

