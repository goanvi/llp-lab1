//
// Created by gav66 on 11/9/2023.
//

#include <stdlib.h>
#include <assert.h>
#include "join.h"

struct join_condition_list *join_builder_init() {
    return NULL;
}

void
join_builder_add(struct join_condition_list **list_p, struct column_description left, struct column_description right) {
    assert(list_p != NULL);
    struct join_condition_list *list = *list_p;
    struct join_condition_list *set = malloc(sizeof(struct join_condition_list));
    if (set != NULL) {
        set->left = left;
        set->right = right;
        set->next = NULL;
        if (list != NULL) {
            struct join_condition_list *join_list = list;
            while (join_list->next != NULL) {
                join_list = list->next;
            }
            join_list->next = set;
        } else {
            *list_p = set;
        }
    }
}

void join_builder_free(struct join_condition_list **list_p) {
    assert(list_p != NULL);
    struct join_condition_list *list = *list_p;
    while (list != NULL) {
        struct join_condition_list *next_inst = list->next;
        free(list);
        list = next_inst;
    }
}
