//
// Created by gav66 on 11/8/2023.
//

#include <assert.h>
#include "cursor.h"

void cursor_free(struct cursor **cur_ptr) {
    assert(cur_ptr != NULL);
    struct cursor * cur = *cur_ptr;
    if (NULL == cur) {
        return;
    }
    cur->free(cur);
    *cur_ptr = NULL;
}

bool cursor_is_empty(struct cursor * cur) {
    assert(cur != NULL);
    return cur->is_empty(cur);
}

void cursor_next(struct cursor * cur) {
    assert(cur != NULL);
    cur->next(cur);
}

void cursor_restart(struct cursor * cur) {
    assert(cur != NULL);
    cur->restart(cur);
}

struct row_column cursor_get(struct cursor * cur, size_t table_idx, size_t column_idx) {
    assert(cur != NULL);
    assert(!cursor_is_empty(cur));
    return cur->get(cur, table_idx, column_idx);
}

void cursor_delete(struct cursor * cur, size_t table_idx) {
    assert(cur != NULL);
    cur->delete(cur, table_idx);
}

void cursor_update(struct cursor * cur, size_t table_idx, struct updater_builder *updater) {
    assert(cur != NULL);
    cur->update(cur, table_idx, updater);
}
