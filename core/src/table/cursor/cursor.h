//
// Created by gav66 on 11/8/2023.
//

#ifndef LLP_CURSOR_H
#define LLP_CURSOR_H

#include <stdbool.h>
#include <stddef.h>
#include "../updater/updater.h"
#include "../table.h"
#include "../join/join.h"

enum cursor_type {
    CURSOR_FROM = 0,
    CURSOR_JOIN = 1,
    CURSOR_WHERE = 2,
};

struct cursor {
    enum cursor_type type;

    void (*free)(struct cursor *cur);

    bool (*is_empty)(struct cursor *cur);

    void (*next)(struct cursor *cur);

    void (*restart)(struct cursor *cur);

    void (*delete)(struct cursor *cur, size_t table_idx);

    void (*update)(struct cursor *cur, size_t table_idx, struct updater_builder *updater);

    struct row_column (*get)(struct cursor *cur, size_t table_idx, size_t column_idx);

    union {
        struct {
            struct table *table;
            struct pool_iter *it;
            size_t table_idx;
            struct row cached_row;
        } from;
        struct {
            struct join_condition_list condition;
            struct cursor *left;
            struct cursor *right;
        } join;
        struct {
            struct where_condition *condition;
            struct cursor *base;
        } where;
    };
};

struct cursor *cursor_init_from(struct table *table, size_t table_idx);

struct cursor *cursor_init_join(struct cursor *left, struct cursor *right, struct join_condition_list condition);

struct cursor *cursor_init_where(struct cursor *base, struct where_condition *condition);

void cursor_free(struct cursor **cur);

bool cursor_is_empty(struct cursor *cur);

void cursor_next(struct cursor *cur);

void cursor_restart(struct cursor *cur);

struct row_column cursor_get(struct cursor *cur, size_t table_idx, size_t column_idx);

void cursor_delete(struct cursor *cur, size_t table_idx);

void cursor_update(struct cursor *cur, size_t table_idx, struct updater_builder *updater);

#endif //LLP_CURSOR_H
