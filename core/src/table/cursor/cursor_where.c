//
// Created by gav66 on 11/8/2023.
//

#include <assert.h>
#include <stdlib.h>
#include "../condition/condition.h"
#include "cursor.h"

static bool column_greater_than(struct row_column first, struct row_column second) {
    assert(first.type == second.type);
    assert(first.type == DATA_TYPE_FLOAT || first.type == DATA_TYPE_INT);
    switch (first.type) {
        case DATA_TYPE_INT:
            return first.value.val_int > second.value.val_int;
        case DATA_TYPE_FLOAT:
            return first.value.val_float > second.value.val_float;
        case DATA_TYPE_STRING:
        case DATA_TYPE_BOOL:
            return false;
    }
}

static bool column_lesser_than(struct row_column first, struct row_column second) {
    assert(first.type == second.type);
    assert(first.type == DATA_TYPE_FLOAT || first.type == DATA_TYPE_INT);
    switch (first.type) {
        case DATA_TYPE_INT:
            return first.value.val_int < second.value.val_int;
        case DATA_TYPE_FLOAT:
            return first.value.val_float < second.value.val_float;
        case DATA_TYPE_STRING:
        case DATA_TYPE_BOOL:
            return false;
    }
}

static bool compare_columns(enum comparing_type compare_type, struct row_column first, struct row_column second) {
    assert(first.type == second.type);
    switch (compare_type) {
        case COMPARE_EQ:
            return columns_equals(first, second);
        case COMPARE_NE:
            return !columns_equals(first, second);
        case COMPARE_GT:
            return column_greater_than(first, second);
        case COMPARE_LT:
            return column_lesser_than(first, second);
        case COMPARE_GE:
            return column_greater_than(first, second) || columns_equals(first, second);
        case COMPARE_LE:
            return column_lesser_than(first, second) || columns_equals(first, second);
    }
}

static struct row_column operand_extract_column(struct operand op, struct cursor *cur) {
    assert(cur != NULL);
    switch (op.type) {
        case OPERAND_VALUE_LITERAL:
            return op.literal;
        case OPERAND_VALUE_COLUMN:
            return cursor_get(cur, op.column.index.table_idx, op.column.index.column_idx);
    }
}

static bool where_condition_check_compare(struct where_condition *condition, struct cursor *cur) {
    assert(cur != NULL);
    assert(condition != NULL && condition->type == CONDITION_COMPARE);
    struct row_column first_column = operand_extract_column(condition->compare.first, cur);
    struct row_column second_column = operand_extract_column(condition->compare.second, cur);
    if (first_column.type != 0 && second_column.type != 0){
        return compare_columns(condition->compare.type, first_column, second_column);
    }
    return false;
}

static bool where_condition_check(struct where_condition *condition, struct cursor *cur) {
    assert(cur != NULL);
    assert(condition != NULL);
    switch (condition->type) {
        case CONDITION_AND:
            return where_condition_check(condition->and.first, cur) &&
                   where_condition_check(condition->and.second, cur);
        case CONDITION_OR:
            return where_condition_check(condition->or.first, cur) ||
                   where_condition_check(condition->or.second, cur);
        case CONDITION_NOT:
            return !where_condition_check(condition->not.first, cur);
        case CONDITION_COMPARE:
            return where_condition_check_compare(condition, cur);
    }
}

static void cursor_free_where(struct cursor *cur) {
    assert(cur != NULL);
    where_condition_free(cur->where.condition);
    cursor_free(&(cur->where.base));
    free(cur);
}

static bool cursor_is_empty_where(struct cursor *cur) {
    assert(cur != NULL);
    return cursor_is_empty(cur->where.base);
}

static void cursor_next_where(struct cursor *cur) {
    assert(cur != NULL);
    struct cursor *base = cur->where.base;
    while (!cursor_is_empty(base) && !where_condition_check(cur->where.condition, cur)) {
        cursor_next(base);
    }
}

static void cursor_restart_where(struct cursor *cur) {
    assert(cur != NULL);
    cursor_restart(cur->where.base);
    while (!where_condition_check(cur->where.condition, cur)) {
        cursor_next(cur->where.base);
    }
}

static struct row_column cursor_get_where(struct cursor *cur, size_t table_idx, size_t column_idx) {
    assert(cur != NULL);
    return cursor_get(cur->where.base, table_idx, column_idx);
}

static void cursor_delete_where(struct cursor *cur, size_t table_idx) {
    assert(cur != NULL);
    cursor_delete(cur->where.base, table_idx);
}

static void cursor_update_where(struct cursor *cur, size_t table_idx, struct updater_builder *updater) {
    cursor_update(cur->where.base, table_idx, updater);
}

struct cursor *cursor_init_where(struct cursor *base, struct where_condition *condition) {
    assert(base != NULL && condition != NULL);
    struct cursor *cur = malloc(sizeof(struct cursor));
    if (cur != NULL) {
        cur->type = CURSOR_WHERE;
        cur->where.condition = condition;
        cur->where.base = base;
        cur->free = cursor_free_where;
        cur->is_empty = cursor_is_empty_where;
        cur->next = cursor_next_where;
        cur->restart = cursor_restart_where;
        cur->get = cursor_get_where;
        cur->delete = cursor_delete_where;
        cur->update = cursor_update_where;
        while (!cursor_is_empty(cur) && !where_condition_check(cur->where.condition, cur)) {
            cursor_next(cur->where.base);
        }
    }
    return cur;
}
