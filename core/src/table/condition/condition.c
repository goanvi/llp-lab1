//
// Created by gav66 on 11/5/2023.
//

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "condition.h"

struct operand operand_column(char *table, char *column) {
    return (struct operand) {
            .type = OPERAND_VALUE_COLUMN,
            .column = {
                    .type = COLUMN_DESC_NAME,
                    .name = {
                            .table_name = table,
                            .column_name = column
                    }
            }
    };
}

struct operand operand_literal_float(float value) {
    return (struct operand) {
            .type = OPERAND_VALUE_LITERAL,
            .literal = {.type = DATA_TYPE_FLOAT, .value = {.val_float = value}}
    };
}

struct operand operand_literal_int(int32_t value) {
    return (struct operand) {
            .type = OPERAND_VALUE_LITERAL,
            .literal = {.type = DATA_TYPE_INT, .value = {.val_int = value}}
    };
}

struct operand operand_literal_bool(bool value) {
    return (struct operand) {
            .type = OPERAND_VALUE_LITERAL,
            .literal = {.type = DATA_TYPE_BOOL, .value = {.val_bool = value}}
    };
}

struct operand operand_literal_string(char *value) {
    return (struct operand) {
            .type = OPERAND_VALUE_LITERAL,
            .literal = {.type = DATA_TYPE_STRING, .value = {.val_string = value}}
    };
}

struct where_condition *where_condition_compare(enum comparing_type type, struct operand op1, struct operand op2) {
    struct where_condition *result = malloc(sizeof(struct where_condition));
    if (result != NULL) {
        result->type = CONDITION_COMPARE;
        result->compare.type = type;
        result->compare.first = op1;
        result->compare.second = op2;
    }
    return result;
}

struct where_condition *where_condition_and(struct where_condition *first, struct where_condition *second) {
    struct where_condition *result = malloc(sizeof(struct where_condition));
    if (result != NULL) {
        result->type = CONDITION_AND;
        result->and.first = first;
        result->and.second = second;
    }
    return result;
}

struct where_condition *where_condition_or(struct where_condition *first, struct where_condition *second) {
    struct where_condition *result = malloc(sizeof(struct where_condition));
    if (result != NULL) {
        result->type = CONDITION_OR;
        result->or.first = first;
        result->or.second = second;
    }
    return result;
}

struct where_condition *where_condition_not(struct where_condition *first) {
    struct where_condition *result = malloc(sizeof(struct where_condition));
    if (result != NULL) {
        result->type = CONDITION_NOT;
        result->not.first = first;
    }
    return result;
}

void where_condition_free(struct where_condition *condition) {
    switch (condition->type) {
        case CONDITION_AND:
            where_condition_free(condition->and.first);
            where_condition_free(condition->and.second);
            break;
        case CONDITION_OR:
            where_condition_free(condition->or.first);
            where_condition_free(condition->or.second);
            break;
        case CONDITION_NOT:
            where_condition_free(condition->not.first);
            break;
        case CONDITION_COMPARE:
            break;
    }
    free(condition);
}

bool columns_equals(struct row_column first, struct row_column second) {
    assert(first.type == second.type);
    switch (first.type) {
        case DATA_TYPE_INT:
            return first.value.val_int == second.value.val_int;
        case DATA_TYPE_FLOAT:
            return first.value.val_float == second.value.val_float;
        case DATA_TYPE_STRING:
            return 0 == strcmp(first.value.val_string, second.value.val_string);
        case DATA_TYPE_BOOL:
            return first.value.val_bool == second.value.val_bool;
    }
}