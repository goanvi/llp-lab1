//
// Created by gav66 on 10/9/2023.
//

#ifndef LLP_FILE_HANDLER_H
#define LLP_FILE_HANDLER_H

#include <stdio.h>
#include <stdint.h>

enum file_status {
    FILE_SUCCESS = 0,
    FAIL_OPEN_FILE,
    FAIL_CLOSE_FILE,
    FAIL_READ_FILE,
    FAIL_WRITE_FILE,
    FAIL_SEEK_FILE
};

enum file_status file_write(FILE *file, int64_t addr, int8_t *buffer, int64_t buffer_size);
enum file_status file_read(FILE *file, int64_t addr, int8_t *buffer, int64_t buffer_size);

#endif //LLP_FILE_HANDLER_H
