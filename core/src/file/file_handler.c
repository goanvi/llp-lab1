//
// Created by gav66 on 10/9/2023.
//
#include "file_handler.h"


enum file_status file_write(FILE *file, int64_t addr, int8_t *buffer, int64_t buffer_size) {
    if (fseek(file, addr, SEEK_SET)) return FAIL_SEEK_FILE;
    size_t size = fwrite(buffer, sizeof(char), buffer_size, file);
    if (size != buffer_size) return FAIL_WRITE_FILE;
    else return FILE_SUCCESS;
}

enum file_status file_read(FILE *file, int64_t addr, int8_t *buffer, int64_t buffer_size) {
    if (fseek(file, addr, SEEK_SET)) return FAIL_SEEK_FILE;
    size_t size = fread(buffer, sizeof(char), buffer_size, file);
    if (size != buffer_size) return FAIL_READ_FILE;
    else return FILE_SUCCESS;
}

FILE *file_open(char *file_name, char *mode) {
    return fopen(file_name, mode);
}

int file_close(FILE *file) {
    return fclose(file);
}
