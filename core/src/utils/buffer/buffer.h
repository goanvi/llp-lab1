//
// Created by gav66 on 10/26/2023.
//

#ifndef LLP_BUFFER_H
#define LLP_BUFFER_H

#include <stdint.h>
#include <stdbool.h>

struct buffer {
    uint64_t size;
    char *data;
    uint64_t rcurs;
    uint64_t wcurs;
};

union u32 {
    int32_t i32;
    uint32_t ui32;
    float f32;
};

union u64 {
    int64_t i64;
    uint64_t ui64;
    double f64;
};

union u8 {
    int8_t i8;
    uint8_t ui8;
};

struct buffer *buffer_init(uint64_t size);

void buffer_free(struct buffer **buffer_pp);

struct buffer *buffer_copy(const struct buffer *buffer);

void buffer_reset(struct buffer *buffer);

bool buffer_is_empty(const struct buffer *buffer);

char *buffer_read_string(struct buffer *buffer);

union u64 buffer_read_b64(struct buffer *buffer);

union u32 buffer_read_b32(struct buffer *buffer);

union u8 buffer_read_b8(struct buffer *buffer);

void buffer_write_string(struct buffer *buffer, const char *string);

void buffer_write_b64(struct buffer *buffer, union u64 num);

void buffer_write_b32(struct buffer *buffer, union u32 num);

void buffer_write_b8(struct buffer *buffer, union u8 num);

struct buffer *buffer_merge(struct buffer *first_buffer, struct buffer *second_buffer);

#endif //LLP_BUFFER_H
