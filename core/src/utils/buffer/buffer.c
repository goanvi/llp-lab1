//
// Created by gav66 on 10/26/2023.
//

#include <stddef.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "buffer.h"


struct buffer *buffer_init(uint64_t size) {
    struct buffer *buffer = NULL;
    buffer = malloc(sizeof(struct buffer));
    if (buffer != NULL) {
        buffer->size = size;
        buffer->rcurs = 0;
        buffer->wcurs = 0;
        buffer->data = NULL;
        if (size > 0) {
            buffer->data = malloc(size);
            if (buffer->data == NULL) {
                return NULL;
            }
        }
    }
    return buffer;
}

void buffer_free(struct buffer **buffer_pp) {
    assert(buffer_pp != NULL);
    struct buffer *buffer = *buffer_pp;
    if (buffer == NULL) {
        return;
    }
    free(buffer->data);
    free(buffer);
    *buffer_pp = NULL;
}

struct buffer *buffer_copy(const struct buffer *buffer) {
    assert(buffer != NULL);
    struct buffer *result = buffer_init(buffer->size);
    memcpy(result->data, buffer->data, buffer->size);
    return result;
}

void buffer_reset(struct buffer *buffer) {
    assert(buffer != NULL);
    buffer->rcurs = 0;
    buffer->wcurs = 0;
}

bool buffer_is_empty(const struct buffer *buffer) {
    assert(buffer != NULL);
    return buffer->rcurs >= buffer->size;
}

char *buffer_read_string(struct buffer *buffer) {
    assert(buffer != NULL);
    size_t length = strlen(buffer->data + buffer->rcurs) + 1;
    uint64_t moved = buffer->rcurs + length;
    if (moved > buffer->size) {
        return NULL;
    }
    char *string = malloc(length);
    memcpy(string, buffer->data + buffer->rcurs, length);
    buffer->rcurs = moved;
    return string;
}

union u64 buffer_read_b64(struct buffer *buffer) {
    assert(buffer != NULL);
    uint64_t moved = buffer->rcurs + sizeof(union u64);
    if (moved > buffer->size) {
        return (union u64) {0};
    }
    union u64 num = *((union u64 *) (buffer->data + buffer->rcurs));
    buffer->rcurs = moved;
    return num;
}

union u32 buffer_read_b32(struct buffer *buffer) {
    assert(buffer != NULL);
    uint64_t moved = buffer->rcurs + sizeof(union u32);
    if (moved > buffer->size) {
        return (union u32) {0};
    }
    union u32 num = *((union u32 *) (buffer->data + buffer->rcurs));
    buffer->rcurs = moved;
    return num;
}

union u8 buffer_read_b8(struct buffer *buffer) {
    assert(buffer != NULL);
    uint64_t moved = buffer->rcurs + sizeof(union u8);
    if (moved > buffer->size) {
        return (union u8) {0};
    }
    union u8 num = *((union u8 *) (buffer->data + buffer->rcurs));
    buffer->rcurs = moved;
    return num;
}

void buffer_write_string(struct buffer *buffer, const char *const string) {
    assert(buffer != NULL && string != NULL);
    size_t length = strlen(string) + 1;
    uint64_t moved = buffer->wcurs + length;
    if (moved > buffer->size) {
        return;
    }
    memcpy(buffer->data + buffer->wcurs, string, length);
    buffer->wcurs = moved;
}

void buffer_write_b64(struct buffer *buffer, union u64 num) {
    assert(buffer != NULL);
    uint64_t moved = buffer->wcurs + sizeof(num);
    if (moved > buffer->size) {
        return;
    }
    union u64 *ptr = (union u64 *) (buffer->data + buffer->wcurs);
    *ptr = num;
    buffer->wcurs = moved;
}

void buffer_write_b32(struct buffer *buffer, union u32 num) {
    assert(buffer != NULL);
    uint64_t moved = buffer->wcurs + sizeof(num);
    if (moved > buffer->size) {
        return;
    }
    union u32 *ptr = (union u32 *) (buffer->data + buffer->wcurs);
    *ptr = num;
    buffer->wcurs = moved;
}

void buffer_write_b8(struct buffer *buffer, union u8 num) {
    assert(buffer != NULL);
    uint64_t moved = buffer->wcurs + sizeof(num);
    if (moved > buffer->size) {
        return;
    }
    union u8 *ptr = (union u8 *) (buffer->data + buffer->wcurs);
    *ptr = num;
    buffer->wcurs = moved;
}

struct buffer *buffer_merge(struct buffer *first_buffer, struct buffer *second_buffer) {
    assert(first_buffer != NULL && second_buffer != NULL);
    uint64_t size_sum = first_buffer->size + second_buffer->size;
    struct buffer * merged_buffer = buffer_init(size_sum);
    if (merged_buffer != NULL){
        memcpy(merged_buffer->data, first_buffer->data, first_buffer->size);
        memcpy(merged_buffer->data + first_buffer->size, second_buffer->data, second_buffer->size);
    }
    return merged_buffer;
}