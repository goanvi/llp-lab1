//
// Created by gav66 on 11/1/2023.
//

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "pool.h"
#include "../../page_chain/page_chain_handler.h"
#include "../../page/page_handler.h"


static int suitable_page_chain_idx(uint64_t size) {
    uint64_t start = POOL_START;
    int count = 0;
    while (start < size) {
        start <<= 1;
        count++;
    }
    return count;
}

int64_t pool_create(struct database *db) {
    assert(db != NULL);
    struct page *page = get_empty_page(db);
    if (page != NULL) {
        int64_t acc = POOL_START;
        struct page_chain_header *chain = malloc(sizeof(struct page_chain_header) * POOL_SIZE);
        if (chain != NULL) {
            for (int i = 0; i < POOL_SIZE; i++) {
                chain[i] = (struct page_chain_header) {
                        .block_size = acc,
                        .first_page_addr = 0,
                        .last_page_addr = 0
                };
                acc *= 2;
            }
            memcpy(page->data + sizeof(struct file_page_header), chain, sizeof(struct page_chain_header) * POOL_SIZE);
            page->header.end_page += sizeof(struct page_chain_header) * POOL_SIZE;
            int64_t page_addr = page->addr;
            stop_using_page(db, &page);
            free(chain);
            return page_addr;
        } else {
            set_empty_page(db, page);
            stop_using_page(db, &page);
        }
    }
    return 0;
}

struct pool *pool_init(struct database *db, int64_t pool_addr) {
    assert(db != NULL);
    struct pool *pool = malloc(sizeof(struct pool));
    if (pool != NULL) {
        pool->page = get_page(db, pool_addr);
        if (pool->page != NULL) {
            struct page_chain_header *chain = NULL;
            for (int i = 0; i < POOL_SIZE; i++) {
                chain = malloc(sizeof(struct page_chain_header));
                if (chain != NULL) {
                    memcpy(chain,
                           pool->page->data + sizeof(struct file_page_header) + (sizeof(struct page_chain_header) * i),
                           sizeof(struct page_chain_header)
                    );
                    pool->chains[i] = chain;
                } else {
                    stop_using_page(db, &pool->page);
                    free(pool);
                    return NULL;
                }
            }
        } else {
            free(pool);
            return NULL;
        }
    }
    pool->db = db;
    return pool;
}


void pool_free(struct pool **pool) {
    assert(pool != NULL && *pool != NULL && (*pool)->page != NULL && (*pool)->db != NULL);
    struct page_chain_header *chains = malloc(sizeof(struct page_chain_header) * POOL_SIZE);
    if (chains != NULL) {
        for (int i = 0; i < POOL_SIZE; i++) {
            chains[i] = *(*pool)->chains[i];
            free((*pool)->chains[i]);
            (*pool)->chains[i] = NULL;
        }
        memcpy((*pool)->page->data + sizeof(struct file_page_header), chains, sizeof(struct page_chain_header) * POOL_SIZE);
        stop_using_page((*pool)->db, &(*pool)->page);
        free(*pool);
        free(chains);
        *pool = NULL;
    }
}

enum pool_status pool_clear(struct pool *pool) {
    assert(pool != NULL && pool->db != NULL);
    for (int i = 0; i < POOL_SIZE; i++) {
        struct page_chain_header *chain = pool->chains[i];
        if (page_chain_clear(pool->db, chain) != CHAIN_SUCCESS) {
            return POOL_CLEAR_ERROR;
        }
    }
    return POOL_SUCCESS;
}

void pool_drop (struct pool **pool_p){
    assert(pool_p != NULL);
    struct pool *pool = *pool_p;
    if (pool == NULL){
        return;
    }
    pool_clear(pool);
    set_empty_page(pool->db, pool->page);
    stop_using_page(pool->db, &pool->page);
    free(pool);
    *pool_p = NULL;
}

enum pool_status pool_append(struct pool *pool, struct buffer *buffer) {
    assert(pool != NULL && pool->page != NULL && buffer != NULL && pool->db != NULL);
    int idx = suitable_page_chain_idx(buffer->size);
    if (page_chain_append(pool->db, pool->chains[idx], buffer) != CHAIN_SUCCESS) {
        return POOL_APPEND_ERROR;
    }
    return POOL_SUCCESS;
}

struct pool_iter *pool_iterator(struct pool *pool) {
    assert(pool != NULL && pool->page != NULL);
    struct pool_iter *it = malloc(sizeof(struct pool_iter));
    if (it != NULL) {
        it->pool = pool;
        it->chain_idx = 0;
        it->chain_it = page_chain_iterator(pool->db, pool->chains[0]);
        if (it->chain_it == NULL) {
            free(it);
            return NULL;
        }
        while (page_chain_is_empty(it->chain_it->chain_header)) {
            it->chain_idx++;
            if (POOL_SIZE == it->chain_idx) {
                it->chain_idx = -1;
                it->chain_it = NULL;
                return it;
            }
            it->chain_it = page_chain_iterator(pool->db, pool->chains[it->chain_idx]);
            if (it->chain_it == NULL) {
                free(it);
                return NULL;
            }
        }
    }
    return it;
}


void pool_iterator_free(struct pool_iter **iter) {
    assert(iter != NULL);
    struct pool_iter *it = *iter;
    if (NULL == it) {
        return;
    }
    page_chain_iterator_free(it->pool->db, &it->chain_it);
    free(it);
    *iter = NULL;
}

bool pool_iterator_is_empty(struct pool_iter *it) {
    assert(it != NULL);
    return it->chain_idx == -1;
}


struct buffer *pool_iterator_get(struct pool_iter *it) {
    assert(it != NULL);
    if (pool_iterator_is_empty(it)) {
        return NULL;
    }
    return page_chain_iterator_get(it->pool->db, it->chain_it);
}


enum pool_status pool_iterator_delete(struct pool_iter *it) {
    assert(it != NULL);
    if (pool_iterator_is_empty(it)) {
        return POOL_DELETE_ERROR;
    }
    if (page_chain_iterator_delete(it->pool->db, it->chain_it) != CHAIN_SUCCESS) {
        return POOL_DELETE_ERROR;
    }
    return POOL_SUCCESS;
}


enum pool_status pool_iterator_next(struct pool_iter *it) {
    assert(it != NULL);
    if (pool_iterator_is_empty(it)) {
        return POOL_ITER_ERROR;
    }
    enum chain_status status = page_chain_iterator_next(it->pool->db, it->chain_it);
    if (status != CHAIN_SUCCESS) {
        if (status == CHAIN_ITER_END) {
            do {
                it->chain_idx++;
                if (POOL_SIZE == it->chain_idx) {
                    it->chain_idx = -1;
                    page_chain_iterator_free(it->pool->db, &it->chain_it);
                    it->chain_it = NULL;
                    return POOL_ITER_END;
                }
                page_chain_iterator_free(it->pool->db, &it->chain_it);
                it->chain_it = page_chain_iterator(it->pool->db, it->pool->chains[it->chain_idx]);
                if (it->chain_it == NULL) {
                    return POOL_ITER_ERROR;
                }
            } while (page_chain_is_empty(it->chain_it->chain_header));
        } else {
            return POOL_ITER_ERROR;
        }
    }
    return POOL_SUCCESS;
}
