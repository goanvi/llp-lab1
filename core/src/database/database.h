//
// Created by gav66 on 10/4/2023.
//

#ifndef LLP_DATABASE_H
#define LLP_DATABASE_H
#define KEY 2143243242

#include "../file/file_handler.h"
#include <stdint.h>
#include <stdio.h>

enum database_init {
    DATABASE_CREATE = 0,
    DATABASE_OPEN
};

enum data_type {
    DATA_TYPE_INT = 1,
    DATA_TYPE_FLOAT = 2,
    DATA_TYPE_STRING = 3,
    DATA_TYPE_BOOL = 4
};

struct header {
    int64_t key;
    int64_t table_metadata;
    int64_t empty_pages;
};

struct database {
    struct header *header;
    FILE *file;
    struct page_list *allocated_pages;
    struct pool *table_pool;
};


struct database *open_db(char *file_name);

struct database *create_db(char *file_name);

enum file_status save_db(struct database **db);

#endif //LLP_DATABASE_H
